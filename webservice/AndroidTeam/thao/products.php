<?php 
    require_once "connect.php";
    $sql = "SELECT * FROM products";
    $datas = $conn->query($sql);
    $productlist = array();
    while ($products = $datas->fetch_object()) {
        array_push($productlist,new product(
            $products->idProducts,
            $products->nameProducts,
            $products->prices,
            $products->note,
            $products->idShops,
            $products->images,
            $products->quantity
        ));
    }
    echo json_encode($productlist);
    //tạo class huong doi tuong
    class product
    {
        function product($id, $name,$price,$note,$idShop,$image,$qty)
        {
            $this->id = $id;
            $this->name=$name;
            $this->price=$price;
            $this->note=$note;
            $this->idShop=$idShop;
            $this->image=$image;
            $this->qty=$qty;
           
        }
    }
?>