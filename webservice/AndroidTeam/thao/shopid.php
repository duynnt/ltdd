<?php 
    require_once "connect.php";
    $id=$_POST["id"];
    $sql = "SELECT * FROM shops WHERE idShops=$id";
    $datas = $conn->query($sql);
    $shoplist = array();
    while ($shops = $datas->fetch_object()) {
        array_push($shoplist,new shop(
            $shops->idShops,
            $shops->nameShops,
            $shops->address,
            $shops->imagesBanner,
            $shops->images,
            $shops->notes,
            $shops->loves
        ));
    }
    echo json_encode($shoplist);
    //tạo class huong doi tuong
    class shop
    {
        function shop($id, $name,$address,$banner,$image,$note,$loves)
        {
            $this->id = $id;
            $this->name=$name;
            $this->address=$address;
            $this->banner=$banner;
            $this->image=$image;
            $this->note=$note;
            $this->loves=$loves;
        }
    }
?>