<?php 
    require_once "connect.php";
    $idshop=$_POST["id"];
    $sql = "SELECT * FROM cart WHERE idShops=$idshop";
    $datas = $conn->query($sql);
    $cartlist = array();
    while ($carts = $datas->fetch_object()) {
        array_push($cartlist,new cart(
            $carts->cartID,
            $carts->productName,
            $carts->Image,
            $carts->qty,
            $carts->price,
            $carts->note_special,
            $carts->idProducts,
            $carts->idShops
        ));
    }
    echo json_encode($cartlist);
    //tạo class huong doi tuong
    class cart
    {
        function cart($id, $name,$image,$qty,$price,$note,$idproduct,$idShop)
        {
            $this->id = $id;
            $this->name=$name;
            $this->image=$image;
            $this->qty=$qty;
            $this->price=$price;
            $this->note=$note;
            $this->idproduct=$idproduct;
            $this->idShop=$idShop;
        }
    }
?>