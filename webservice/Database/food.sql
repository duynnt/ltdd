-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2020 at 08:27 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `food`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id_address` int(11) NOT NULL,
  `address_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id_address`, `address_name`, `address`, `address_details`, `note`) VALUES
(7, 'Nha rieng', 'Charleston & Huff, Mountain View, CA 94043, USA', '162 Động da, Hai cháu, Da Nang', 'đi nhanh len');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cartID` int(11) NOT NULL,
  `productName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Image` text COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(255) NOT NULL,
  `price` int(50) NOT NULL,
  `note_special` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idProducts` int(11) NOT NULL,
  `idShops` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cartID`, `productName`, `Image`, `qty`, `price`, `note_special`, `idProducts`, `idShops`) VALUES
(1, 'Bánh Tráng Trộn', 'https://res.klook.com/image/upload/v1596008298/blog/eibedalo0wncojkerkpg.jpg', 1, 15000, 'Vd:Không hành,...', 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `codeuudai`
--

CREATE TABLE `codeuudai` (
  `IdCode` int(11) NOT NULL,
  `MaCode` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `endows`
--

CREATE TABLE `endows` (
  `code` varchar(50) NOT NULL,
  `nameEndow` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`id`, `name`, `phone`, `email`) VALUES
(1, 'Nguyễn Thanh', '0368604577', 'duynguyennnt@gmail.com'),
(2, 'Duy', '13456', 'dn000@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idProducts` int(11) NOT NULL,
  `nameProducts` varchar(255) NOT NULL,
  `prices` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `idTypes` int(11) NOT NULL,
  `idShops` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`idProducts`, `nameProducts`, `prices`, `note`, `idTypes`, `idShops`, `images`, `quantity`) VALUES
(1, 'Trà Sữa Okinawa hạt dẹp đường đen', 38000, '', 1, 2, 'https://dayphache.edu.vn/wp-content/uploads/2020/02/mon-tra-sua-tran-chau.jpg', 2),
(2, 'Trà Sữa Trân Châu', 32000, '', 1, 1, 'https://cdn.cet.edu.vn/wp-content/uploads/2018/04/tra-sua-tu-lam.jpg', 1),
(3, 'Cơm gà', 34000, '', 2, 4, 'https://images.foody.vn/res/g11/101185/prof/s576x330/foody-mobile-gvww-jpg-603-636136802563729629.jpg', 1),
(4, 'cơm ba chỉ giòn da', 40000, '', 2, 4, 'https://media.foody.vn/res/g11/101185/s/foody-com-chien-gion-gia-vinh-542-635558090702511882.JPG', 1),
(5, 'Cơm gà xé', 43000, '', 2, 4, 'https://images.foody.vn/res/g21/209552/s/foody-com-chien-gion-gia-vinh-2-209552-889-635900328135564379.jpg', 1),
(6, 'Cơm chiên dương châu', 30000, '', 2, 4, 'https://images.foody.vn/res/g21/209552/s760/foody-com-chien-gion-gia-vinh-2-492-636548130825059888.jpg', 1),
(7, 'Trà sữa đặc biệt', 32000, '', 1, 6, 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/VNGFVN0000090b/hero/2727747fe11f4a9fb8884333c4b73c9a_1598803921810533086.jpg', 1),
(8, 'Tokuki - Nhật bản', 21000, '', 3, 2, 'https://images.foody.vn/res/g10/97958/s750/foody-an-vat-a-972-636590182922129252.jpg', 1),
(9, 'Cá viên chiên ', 50000, '', 3, 2, 'https://images.foody.vn/res/g104/1033768/prof/s1242x600/foody-upload-api-foody-mobile-foody-upload-api-foo-200701125149.jpg', 1),
(10, 'Bánh Tráng Trộn', 15000, '', 3, 2, 'https://res.klook.com/image/upload/v1596008298/blog/eibedalo0wncojkerkpg.jpg', 1),
(11, 'Bánh tráng nướng', 16000, '', 3, 2, 'https://www.taidanang.com/wp-content/uploads/2017/04/an-vat-Da-Nang-.jpg', 1),
(12, 'Chè Thái', 21000, '', 3, 3, 'https://toplist.vn/images/800px/che-sau-duong-nguyen-van-linh-276949.jpg', 1),
(13, 'Cơm cháy', 31000, '', 3, 3, 'https://cdn.vietnammoi.vn/2019/9/28/photo-1-1569641763425946640299.jpg', 1),
(14, 'Bánh cá', 23000, '', 3, 3, 'https://bcasolutions.vn/wp-content/uploads/2020/08/1-18.jpg', 1),
(15, 'Ram - Chả cuốn', 63000, '', 3, 3, 'https://res.klook.com/image/upload/v1596028598/blog/rsh3osfrpftjjpddll0t.jpg', 1),
(16, 'Trà sữa truyền thống', 24000, '', 1, 1, 'https://img-global.cpcdn.com/recipes/c49ae39740dd3e15/751x532cq70/tra-s%E1%BB%AFa-truy%E1%BB%81n-th%E1%BB%91ng-recipe-main-photo.jpg', 1),
(17, 'Trà sữa trái cây', 21000, '', 1, 1, 'https://image.thanhnien.vn/800/uploaded/minhnguyet/2016_12_07/tra-sua_hwnb.jpg', 1),
(18, 'Bánh mì thịt nướng ', 13000, '', 3, 5, 'https://assets.grab.com/wp-content/uploads/sites/11/2019/07/24151853/E994FAA3-4B56-4A89-93FC-0B132BCBF4BC-e1563952743106.jpg', 1),
(19, 'Bánh mì kẹp', 10000, '', 3, 5, 'https://cdn.tgdd.vn/Files/2020/06/03/1260530/cach-lam-banh-mi-tam-giac-tho-nhi-ky-doner-kebab-11.jpg', 1),
(20, 'Bánh mì pate', 8000, '', 3, 5, 'https://phuot3mien.com/wp-content/uploads/2020/03/banh-mi-phuong-hoi-an.jpg', 1),
(21, 'Hamberger', 15000, '', 3, 5, 'https://images.foody.vn/res/g80/794391/prof/s1242x600/foody-upload-api-foody-mobile-10-190314082605.jpg', 1),
(22, 'Mì cay thực cẩm', 40000, '', 3, 2, 'https://beptruong.edu.vn/wp-content/uploads/2018/06/mi-cay-han-quoc.jpg', 1),
(23, 'Mì cay cấp độ 7', 39000, '', 3, 3, 'https://shipdoandemff.com/wp-content/uploads/2018/05/M%C3%AC-cay-h%E1%BA%A3i-s%E1%BA%A3n.jpg', 1),
(24, 'Cafe Phố', 12000, '', 4, 8, 'https://i1.wp.com/blog.kamereo.vn/wp-content/uploads/2019/05/fernando-hernandez-1422161-unsplash.jpg?resize=930%2C620&ssl=1', 1),
(25, 'Cafe đặc biệt', 20000, '', 4, 8, 'https://capheducnguyen.com/storage/public/user-1/ca-phe-saigon.jpg', 1),
(26, 'Lẩu hải sản', 130000, '', 5, 9, 'https://botania.com.vn/upload/files/anh.jpg', 1),
(27, 'Cua càng xanh', 60000, '', 5, 9, 'https://dulichkhampha24.com/wp-content/uploads/2020/09/quan-hai-san-hai-phong-1.jpg', 1),
(29, 'Lẩu hải sản', 145000, '', 5, 9, 'https://login.medlatec.vn/ImagePath/imageslead/20191027/20191027_gan-nhiem-mo-co-nen-an-hai-san-khong-1.jpg', 1),
(30, 'Cua càng xanh', 67000, '', 5, 9, 'https://dulichkhampha24.com/wp-content/uploads/2020/09/quan-hai-san-hai-phong-1.jpg', 1),
(31, 'Hải sản tươi sống', 190000, '', 5, 9, 'https://static.salekit.vn/image/shop/2/source/kinh-doanh-hai-san-tuoi-song3.jpg', 1),
(32, 'Trà sữa Bông', 32000, '', 1, 1, 'https://images.foody.vn/res/g71/700532/prof/s1242x600/foody-upload-api-foody-mobile-14a-200213134340.jpg', 1),
(33, 'Trà sữa truyền thống', 19000, '', 1, 1, 'https://images.foody.vn/res/g99/989671/s800/foody-bong-food-drink-dien-bien-phu-330-637169525977549199.jpg', 1),
(34, 'Trà sữa nhà trân châu', 32000, '', 1, 1, 'https://d1sag4ddilekf6.cloudfront.net/compressed/items/5-CZJJGKNTGF52JN-CZJJGKNTN62CLT/photo/c64b509c9f8b4f1497f6add8f20f458f_1600080051949873600.jpg', 1),
(35, 'Trà sữa nha đam', 35000, '', 1, 1, 'https://jarvis.vn/uploaded/tra-sua-chan-trau.jpg', 1),
(36, 'Trà Sữa Hồng', 21000, '', 1, 1, 'https://monngonmoingay.com/wp-content/uploads/2019/03/tra-sua-tran-chau-duong-den-500.jpg', 1),
(37, 'Trà Đào', 17000, '', 1, 1, 'https://images.foody.vn/res/g24/235611/prof/s576x330/foody-mobile-20160401_5102ac08e1a-682-635990142048731693.jpg', 1),
(38, 'Trà sữa hồng đào', 42000, '', 1, 1, 'https://images.foody.vn/video/s800x450/foody-upload-api-foody-untitled-1-636579098619124794-180329084414.jpg', 1),
(39, 'Trà soda kem', 22000, '', 1, 1, 'https://hocphache.com.vn/wp-content/uploads/2018/05/topping-nhieu-mau-sac.jpg', 1),
(40, 'Trà Thạch', 29000, '', 1, 1, 'https://nguyenlieuphachehanoi.com/wp-content/uploads/2019/06/ten-loai-tra-sua-duoc-san-don-nhat-hien-nay-hong-tra-sui-bot.4.jpg', 1),
(41, 'Royaty', 34000, '', 1, 1, 'https://luankha.com/wp-content/uploads/2019/09/29-1.jpg', 1),
(42, 'Ốc bưu hấp xả', 63000, '', 3, 2, 'https://luhanhvietnam.com.vn/du-lich/vnt_upload/news/05_2020/an-vat-o-dau-ngon-hung-yen.jpg', 1),
(44, 'Ôc hút ran', 90000, '', 3, 2, 'https://kenhthoitiet.vn/wp-content/uploads/2019/05/mon-an-vat-ha-noi.jpg', 1),
(45, 'Cá viên tươi chiên', 78000, '', 3, 2, 'https://kiotviet.vn/wp-content/uploads/2019/10/kinh-doanh-%C4%91%E1%BB%93-%C4%83n-v%E1%BA%B7t-m%C3%B9a-%C4%91%C3%B4ng.jpg', 1),
(46, 'Thịt xiêng nướng', 64000, '', 3, 2, 'https://i-kinhdoanh.vnecdn.net/2015/10/31/kinhdoanhdoanvatonline1-2855-1446282923.jpg', 1),
(47, 'Ốc móc tay hấp xả', 67000, '', 3, 2, 'https://image.thanhnien.vn/800/uploaded/congthang/2020_04_26/anh6anvat_vgzv.jpg', 1),
(48, 'Bánh xèo - Thịt lụi', 90000, '', 3, 2, 'https://ghv.com.vn/images/news/2019/3/hh_copy1.png', 1),
(49, 'Ram cuốn cải', 32000, '', 3, 3, 'https://halotravel.vn/wp-content/uploads/2020/06/angi.in_.hanoi_75403216_1217009435154376_2193173697146219049_n.jpg', 1),
(50, 'Ăn vặt combo', 37000, '', 3, 3, 'https://xebanhmithonhiky.vn/wp-content/uploads/2020/09/ban-do-an-vat-cho-hoc-sinh.jpg', 1),
(51, 'Bánh bột lọc', 21000, '', 3, 3, 'https://vcdn-dulich.vnecdn.net/2020/07/09/3-4200-1594290042.jpg', 1),
(52, 'Bánh bía', 66000, '', 3, 3, 'https://dacsanvietnam.co/wp-content/uploads/2019/05/Top-19-M%C3%B3n-%C4%83n-v%E1%BA%B7t-ngon-%E1%BB%9F-Hu%E1%BA%BF-v%C3%A0-20-Qu%C3%A1n-%C4%83n-v%E1%BA%B7t-n%E1%BB%95i-ti%E1%BA%BFng.-600x381.jpg', 1),
(53, 'Lòng xào', 54000, '', 3, 3, 'https://phatkimthanh.com/mon-an-vat-hot-trend-1.jpg', 1),
(54, 'Bắp hấp bia', 31000, '', 3, 3, 'https://media.cooky.vn/images/blog-2016/20-mon-an-vat-ngon-bo-re-duoi-20-000-dong%202.jpg', 1),
(55, 'Bánh bèo', 10000, '', 3, 3, 'https://image.thanhnien.vn/768/uploaded/congthang/2020_04_26/anh15anvat_hynd.jpg', 1),
(56, 'Bánh tráng trộn', 12000, '', 3, 3, 'https://content.baydep.vn/Upload/Images/Content/anvat1(2).jpg', 1),
(57, 'Ốc cua đồng hấp', 21000, '', 3, 3, 'https://top247.vn/wp-content/uploads/2020/06/top-5-quan-an-vat-thu-duc-hap-dan-khong-the-bo-qua-4.jpg', 1),
(58, 'Cơm phần món', 22000, '', 2, 4, 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/5-CZAWA2JJJCMDTX/hero/61de40a4e7004718a29a4a4475361f0d_1598806082517969930.jpg', 1),
(59, 'Cơm hến huế', 14000, '', 2, 4, 'https://danang.huongnghiepaau.com/images/mon-ngon-mien-trung/cach-lam-com-hen.jpg', 1),
(60, 'Cơm cá khô - đậu', 23000, '', 2, 4, 'https://cdn.pastaxi-manager.onepas.vn/content/uploads/articles/nguyenhuong/quancomhuenhu/quan-com-hue-nhu-thai-phien-da-nang-5.jpg', 1),
(61, 'Cơm Gà Đùi', 45000, '', 2, 4, 'https://images.foody.vn/res/g21/209552/s180x180/foody-com-chien-gion-gia-vinh-2-644-636019759828926326.jpg', 1),
(62, 'Cơm Gà 1/2 con', 90000, '', 2, 4, 'https://images.foody.vn/res/g11/101185/s180x180/foody-com-chien-gion-gia-vinh-941-635899497813136898.jpg', 1),
(63, 'Cơm Gà Xé', 440000, '', 2, 4, 'https://images.foody.vn/res/g11/101185/s180x180/foody-com-chien-gion-gia-vinh-591-636108240598463398.jpg', 1),
(64, 'cơm Ất gà', 32000, '', 2, 4, 'https://media.foody.vn/res/g11/101185/s640x640/foody-com-chien-gion-gia-vinh-833-635802855445956412.jpg', 1),
(65, 'Cơm Xá xíu', 43000, '', 2, 4, 'https://images.foody.vn/res/g11/101185/s750/foody-com-chien-gion-gia-vinh-555-636591469920847592.jpg', 1),
(66, 'Bánh mì không', 10000, '', 3, 5, 'https://product.hstatic.net/1000313040/product/18-56_cba911d93753423f89e172122597d548_master.jpg', 1),
(67, 'Bánh Mì Ngọt', 23000, '', 3, 5, 'https://www.hoteljob.vn/files/Anh-HTJ-Hong/cach-tao-hinh-banh-mi-8.jpg', 1),
(68, 'Bánh mì Than', 34000, '', 3, 5, 'https://savourebakery.com/vnt_upload/news/07_2020/banh-mi-than.jpg', 1),
(69, 'Bánh mì thường', 9000, '', 3, 5, 'https://shipdoandemff.com/wp-content/uploads/2018/05/B%C3%A1nh-m%E1%BB%B3-g%C3%A0-B%C3%A0-Lan.jpg', 1),
(70, 'Bành mì thanh long', 22000, '', 3, 5, 'https://cdn.tgdd.vn/Files/2020/06/19/1264063/cach-lam-banh-mi-thanh-long-nhan-pho-mai-thom-ngon-7.jpg', 1),
(71, 'Bánh mì không thanh long', 8000, '', 3, 5, 'https://media-cdn.laodong.vn/Storage/NewsPortal/2020/2/26/786937/Banhbao-5-1582557237.jpg', 1),
(72, 'Bánh kẹp thịt', 23000, '', 3, 5, 'https://img-global.cpcdn.com/recipes/8ad37a8cb6823eb0/1200x630cq70/photo.jpg', 1),
(73, 'sandwich kẹp', 21000, '', 3, 5, 'https://miro.medium.com/max/540/0*qWJNUF97AeV_L-wn.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `id` int(11) NOT NULL,
  `nameSale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imageSale` text COLLATE utf8_unicode_ci NOT NULL,
  `backgroundSale` text COLLATE utf8_unicode_ci NOT NULL,
  `dayStart` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dayFinish` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descriptionSale` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`id`, `nameSale`, `imageSale`, `backgroundSale`, `dayStart`, `dayFinish`, `descriptionSale`) VALUES
(1, 'Miễn phí giao hàng 20K', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKjtJokNdrYi8tfwVietzq_vx0OuRbVR6-Qg&usqp=CAU', 'https://daygiare.com/public/storage/thumb/2020/02/05/Khuyen-Mai-GrabFood-Cho-Bua-Buffet-Chi-Tu-K-Tai-Eleven.png', '19 Nov 2020', '31 Dec 2020', '-Ưu đãi miễn phí giao hàng tối đa 20.000đ cho đơn hàng từ 40.000đ.\r\n-Ưu đĩa chỉ áp dụng cho các của hàng có Stiker của chương trình.\r\n-Chỉ áp dụng trên phí vận chuyển, không áp dụng trên giá đơn hàng.\r\n-Số lượng khuyến mãi giới hạn.'),
(2, 'Moca | Ưu đĩa 60% tối đa 40K', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPSyj6IQfCrisZqBSQpzNE0W38UWN_mMgYaQ&usqp=CAU', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFMWt_HoB_iRPy0ppK811Zq2OnaXLqkv-oIA&usqp=CAU', '10 Dec 2020', '16 Dec 2020', '-Ưu đãi 60% tối đa 40.000đ x3 đươn hàng Grabfood.\r\n-Áp dụng tối đa 1 lần 1 ngày.\r\n-Áp dụng cho đơn hàng thanh toán qua Thẻ hoặc ví điện tử Moca trên ứng dụng Grab đầu tiên.\r\n-Ưu đãi chỉ áp dụng cho Quán ưa thích - Nhà hàng có dấu tích xanh và GrabKitchen, không áp dụng cho GrabMart.'),
(3, 'Miễn phí giao hàng đơn từ 50K', 'https://vietnamsautaylai.com/wp-content/uploads/2019/11/grab-la-gi-1280x720.jpg', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuQu2wbDikowMhuMRFdC18nIQAZuLxjayxfg&usqp=CAU', '14 Dec 2020', '20 Dec 2020', '-Ưu dãi Miễn PHÍ Giao Hàng 15.000đ cho đơn hàng từ 50.000đ.\r\n-Áp dụng cho Quán ưu thích-Nhà hàng có dấu tích xanh trên GrabFood.\r\n-Chỉ áp dụng trên giá trị vận chuyển, không áp dụng cho giá trị đơn hàng.\r\n-Số lượng khuyễn mãi có giới hạn.'),
(4, 'Ưu đãi 25K đơn từ 80K', 'https://img.kam.vn/images/414x0/1254983c12f348a2abdc2997bcd3fcb0/image/grabfood-bua-sang-tiet-kiem-chi-tu-25k.jpg', 'https://www.tiendauroi.com/wp-content/uploads/2020/10/Cover-Image-VODIEUKIEN.jpg', '14 Dec 2020', '20 Dec 2020', '-Ưu đĩa 25.000đ cho đơn hàng từ 80.000đ.\r\n-Áp dụng cho Quán ưa thích-Nhà hàng có dấu tích xanh chọn lọc.\r\n-Áp dụng trên giá đơn hàng, không áp dụng trên phí vận chuyển.\r\n-Mỗi khách hàng áp dụng dụng tối đa 2 lần.\r\n-Số lượng khuyễn mãi giới hạn.'),
(5, 'Ưu đãi 40K GrabFood MasterCard', 'https://img.kam.vn/images/414x0/03282e37bc6b4be394b43adff5fcee37/image/kfc-ma-mien-phi-giao-hang-grabfood-den-40k.jpg', 'https://assets.grab.com/wp-content/uploads/sites/11/2020/07/10002915/Blog-Header-27-1.jpg', '14 Dec 2020', '20 Dec 2020', '-Áp dụng cho loại hình dịch vụ GrabFood\r\n-Ưu đãi cso giá trị giảm 40.000đ cho đơn hàng từ 100.000đ.\r\n-Mỗi khách hàng được sử dụng 2 lượt nhập mã MASTERCARD40 trong 1 tuần.\r\n-Mỗi khách hàng được sử dụng tối đa 1 lượt nhập mã MASTERCARD40 trong 1 ngày.\r\n-Không sử dụng cùng với các khuyễn mãi khác của Grab\r\n-Mã ưu đãi không bao gồm phí dochj vụ Ứng dụng.\r\n-Ưu đãi áp dụng cho tất cả khách hàng thanh toán trực tiếp bằng thẻ MasterCard (thẻ ghi nơ, thẻ tín dụng và thẻ trả trước) trên ứng dụng Grab của ngân hàng cung cấp bới Grab.\r\n*Các sản phẩm và dịch vụ được cung cấp bởi grab. MasterCard không chịu bất kỳ trách nhiệm nào liên quan đến các sản phẩm và dịch vụ.\r\n-Chỉ áp dụng trên giá trị đơn hàng, không áp dụng trên phí giao hàng.\r\n-Số lượng khuyễn mãi giới hạn mỗi ngày.'),
(6, 'Miễn phí giao hàng 15K', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD_-DmJBNiCS34Ci8mjgeXcxNBymODJttC8w&usqp=CAU', 'https://www.tiendauroi.com/wp-content/uploads/2020/05/96809845_2790257911205737_4094228689317789696_o-750x750.jpg', '14 Dec 2020', '20 Dec 2020', '-Ưu đãi miễn phí giao hàng 15.000đ.\r\n-Chỉ áp dụng Quán ưa thích-Nhà hàng có dấu tích xanh chọn lọc.\r\n-Chỉ áp dụng trên phí vận chuyển, không áp dụng trên giá đươn hàng.\r\n-Mỗi khách hàng chỉ áp dụng tối đa 1 lần 1 ngày, 2 lần tuần.\r\n-Số lượng khuyễn mãi giới hạn.'),
(7, 'Miễn phí giao hàng đơn từ 50K', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD_-DmJBNiCS34Ci8mjgeXcxNBymODJttC8w&usqp=CAU', 'https://www.tiendauroi.com/wp-content/uploads/2020/05/96809845_2790257911205737_4094228689317789696_o-750x750.jpg', '14 Dec 2020', '31 Dec 2020', '-Ưu đãi Miễn phí Giao Hàng 15.000đ cho đươn hàng từ 50.000đ.\r\n-Áp dụng cho Quán ưu thích-Nhà hàng có dấu tích xanh trên GrabFood.\r\n-chỉ áp dụng trên giá phí vận chuyển , không áp dụng cho giá trị đơn hàng.\r\n-Số lượng khuyễn mĩa có giới hạn.A'),
(8, 'ATM | Ưu đãi 70% tối đa 40%', 'https://img.kam.vn/images/414x0/f52ff04c73f042ddb6bf20a326f5803e/image/pvcombank-tang-100k-grabcar-bike-grabfood-cho-chu-the-atm-pvcombank.jpg', 'https://assets.grab.com/wp-content/uploads/sites/11/2020/08/27161852/1200-x-660-Cashles-Thursday.png', '10 Dec 2020', '26 Dec 2020', '-Ưu đãi 70% tối đa 40.000đ x3 đơn hàng GrabFood\r\n-Chương trình \"GẮN THẺ DỄ DÀNG\" diễn ra từ 11/12/2020 - 31/12/2020. Áp dụng cho các khách hàng lần đầu tiên liên kết thẻ ATM ngân hàng vào ứng dụng Grab trong khoản thời gian trên.\r\n-Áp dụng cho khách hàng lần đầu tiên thanh toán qua Thẻ hoặc ví điện tử Moca qua ứng dụng Grab.');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `idShops` int(11) NOT NULL,
  `nameShops` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `imagesBanner` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `loves` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`idShops`, `nameShops`, `address`, `imagesBanner`, `images`, `notes`, `loves`) VALUES
(1, 'Lotteria-Nguyễn Thị Minh Khai', '102/2 Nguyễn Thị Minh Khai', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtGlJaMzypZjC_0IF5bg0SRsFnblF76-ck1Q&usqp=CAU', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8nSxVxt5dEahG2eRuOAG3kbWaxSUyMIkdww&usqp=CAU', 'km', 0),
(2, 'Tiệm ăn Nguyễn Văn Linh', '230 Nguyễn Văn Linh', 'https://scx2.b-cdn.net/gfx/news/hires/2016/howcuttingdo.jpg', 'https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/article_thumbnails/slideshows/great_food_combos_for_losing_weight_slideshow/650x350_great_food_combos_for_losing_weight_slideshow.jpg', '', 0),
(3, 'Falyn - Trà sữa & ăn vặt - Lý tự trong', '12 Lý Tự Trọng', 'https://miro.medium.com/max/699/0*1CPVBLFLkQC8yfTi.jpg', 'https://i.pinimg.com/736x/60/de/7f/60de7f8fc369c1f4b023360c3c0f279a.jpg', '', 0),
(4, 'Gia Vĩnh Cơm Gà', '12 Lý Tự Trọng', 'https://images.foody.vn/res/g103/1025566/prof/s1242x600/foody-upload-api-foody-mobile-691239ce-8a24-4f09-9-201007144917.jpg', 'https://images.foody.vn/res/g11/101185/s180x180/foody-com-chien-gion-gia-vinh-368-636318677301493829.jpg', '', 1),
(5, 'Bánh Mì Bà Lan', '10 Nguyễn Thị Minh Khai', 'https://i.ytimg.com/vi/P3FMSDEN8b4/maxresdefault.jpg', 'https://assets.grab.com/wp-content/uploads/sites/11/2020/01/20104603/photo9-40-e1579488381696.jpg', '', 1),
(6, 'Tiger Sugar Delivery - Đống Đa', '309 Đống Đa', 'https://images.foody.vn/res/g77/761834/s/foody-tiger-sugar-tra-sua-dai-loan-381-636681038356105899.jpg', 'https://d1sag4ddilekf6.cloudfront.net/item/VNITE2019082108162117049/photos/953ae0e059c34827acc904fcb2cfcde2_1593257328127133242.jpg', '', 0),
(7, 'House of Cha', '54 Hùng Vương', 'https://media.foody.vn/res/g95/943026/prof/s/foody-upload-api-foody-mobile-60-hong-tra-sua-dao-190726103956.jpg', 'https://channel.mediacdn.vn/prupload/608/2018/04/img20180405094241983.jpg', '', 0),
(8, 'Ka Cộng Cafe', '30 Hàm Nghi', 'https://cdn3.ivivu.com/2019/06/quan-cafe-sai-gon-ivivu-12.jpg', 'https://kenh14cdn.com/2016/img-6922-copy-1479496162999.jpg', 'km', 1),
(9, 'Quán Bà 8 - Hải sản tươi sống', '39 Ông ích khiêm', 'https://vinmec-prod.s3.amazonaws.com/images/20190622_053808_361264_hai-san.max-800x800.jpg', 'https://vinmec-prod.s3.amazonaws.com/images/20200410_160048_715088_hai-san-co-tot-cho-.max-1800x1800.jpg', '', 0),
(10, 'Cơm Huế Như', '32 Ngô Gia Tự', 'https://images.foody.vn/res/g96/959667/s800/foody-com-hue-nhu-ngo-gia-tu-158-637173814195594981.jpg', 'https://images.foody.vn/res/g96/959667/s800/foody-com-hue-nhu-ngo-gia-tu-132-637173810683874062.jpg', 'km', 0),
(11, 'Mì Quảng Gà', '122 Lê Duẩn', 'https://beptruong.edu.vn/wp-content/uploads/2013/08/ech-lam-mi-quang.jpg', 'https://i.ytimg.com/vi/TTWTwhV9Zx0/maxresdefault.jpg', '', 0),
(12, 'Cháo Xôi Bà Lan', '102 Đống Đa', 'https://images.foody.vn/res/g24/238060/prof/s1242x600/foody-mobile-ch-vit-jpg-736-635997936073010026.jpg', 'https://media.cooky.vn/recipe/g1/4027/s480x480/recipe4027-prepare-step5-635718691965609697.jpg', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `idTypes` int(11) NOT NULL,
  `nameTypes` varchar(255) NOT NULL,
  `idShops` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`idTypes`, `nameTypes`, `idShops`) VALUES
(1, 'Trà Sữa', 1),
(2, 'Cơm', 2),
(3, 'Ăn vặt', 3),
(4, 'Cafe', 5),
(5, 'Hải sản', 6),
(6, 'Mỳ', 7),
(8, 'Cháo', 9),
(10, 'Giải khát', 11);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUsers`, `username`, `password`) VALUES
(1, 'admin', 'admin'),
(2, 'duyne', '123'),
(3, '', ''),
(4, '', ''),
(5, 'duy11', '123'),
(6, '0368604577', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `uudai_product`
--

CREATE TABLE `uudai_product` (
  `idsp` int(11) NOT NULL,
  `productName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(50) NOT NULL,
  `idshop` int(50) NOT NULL,
  `idType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uudai_product`
--

INSERT INTO `uudai_product` (`idsp`, `productName`, `Image`, `price`, `idshop`, `idType`) VALUES
(1, 'MilkeTea Gongcha', 'https://soyagarden.com/content/uploads/2019/12/28122019_SOYA4933-683x1024.jpg', 20000, 1, 1),
(2, 'MilkTea Xam', 'https://3.bp.blogspot.com/-WUDfXv1sgyc/WLO-huMPz0I/AAAAAAAAACE/HDTn96MUw_golzz3xg01ROvvUy7ceHzqwCLcB/s1600/in-ly-nhua-tra-sua.jpg', 12000, 4, 1),
(3, 'MilkTea Kem', 'https://i.pinimg.com/originals/17/d8/0a/17d80afda3d109f4ba5d9c48fbc00819.jpg', 30000, 5, 1),
(4, 'MilkTea nhà làm', 'https://s3-media4.fl.yelpcdn.com/bphoto/7b7SCxE8Z4W6Tu3eBbK8rA/348s.jpg', 20000, 5, 1),
(5, 'MilkTea Coco', 'https://posapp.vn/wp-content/uploads/2019/07/bot-tra-sua.png', 15000, 5, 1),
(6, 'MilkTea Bubble', 'https://picdn.gomaji.com/products/o/606/253606/253606_4.jpg', 15000, 5, 1),
(7, 'MilkTea Coco', 'https://th.bing.com/th/id/OIP.gxUeQD33UU4j3Dc4wFK56AHaGP?pid=Api&w=510&h=430&rs=1', 15000, 5, 1),
(8, 'MilkTea Coco', 'https://cdn01.diadiemanuong.com/ddau/640x/phat-them-voi-5-quan-tra-sua-sang-chanh-ship-tan-nha-d1d44840636015096017259492.jpg', 15000, 5, 1),
(9, 'Kem Gongcha', 'https://2.bp.blogspot.com/-XY0VgQPblDI/VgoRzgJAwVI/AAAAAAAAAQU/E7iWnzXhEdI/s1600/kem-ngon.jpg', 15000, 2, 4),
(10, 'Kem Xam', 'https://th.bing.com/th/id/OIP.vgAr4WupVlJt1M2YRXr3dQEsEs?pid=Api&rs=1', 10000, 4, 4),
(11, 'Kem  Ốc Quế', 'https://www.ladybehindthecurtain.com/wp-content/uploads/2019/08/Monster-Cookie-Ice-Cream-2.jpg', 8000, 2, 4),
(12, 'Kem ly ', 'https://4.bp.blogspot.com/_lqE2cygBFbk/THKsjbnahzI/AAAAAAAABOg/wqTOYTjPFKA/s1600/chocpbicecream.jpg', 5000, 3, 4),
(13, 'Kem Thạch Dừa', 'https://thefirstyearblog.com/wp-content/uploads/2016/06/Oreo-Ice-Cream-17-600x903.jpg', 15000, 3, 4),
(14, 'Kem Bubble', 'https://mymodernmet.com/wp/wp-content/uploads/2017/06/rainbow-ice-cream-tacos-sweet-cup-8.jpg', 15000, 6, 4),
(15, 'Kem Coco', 'https://www.momsandmunchkins.ca/wp-content/uploads/2017/05/ice-cream-nachos-1.jpg', 15000, 5, 4),
(16, 'Mì cay', 'https://bloganchoi.com/wp-content/uploads/2016/11/mi-cay-4.jpg', 15000, 2, 2),
(17, 'Mì cay nướng', 'https://media.foody.vn/res/g20/194867/s/foody-mi-cay-naga-849-635864221701312033.jpg', 10000, 4, 2),
(18, 'Mì cay xào', 'https://media.foody.vn/res/g20/194867/s/foody-mi-cay-naga-849-635864221701312033.jpg', 2000, 2, 2),
(19, 'Mì cay Hàn Quốc', 'https://images.foody.vn/res/g19/186233/s/foody-mi-cay-yagami---tpcl-186233-438-635832685184574426.jpg', 5000, 3, 2),
(20, 'Mì cay hảo hạn', 'https://chibi.info/wp-content/uploads/2017/09/mi-cay-han-quoc.jpg', 15000, 3, 2),
(23, 'Kim bap hộp', 'https://dohyunkim183.files.wordpress.com/2013/10/kimbap.jpg', 15000, 6, 3),
(24, 'Kim bap nướng', 'https://shameleonskitchen.com/wp-content/uploads/2019/02/kimbap1-3000x1953.jpg', 20000, 7, 3),
(25, 'Kim bap HÀn quốc', 'https://s-media-cache-ak0.pinimg.com/originals/81/a4/4d/81a44dea8e8703b48e1ae5241efc1772.jpg', 5000, 5, 3),
(26, 'Kim bap nhà làm', 'https://th.bing.com/th/id/OIP.nclHunNGOn4IlIodEncLXgAAAA?pid=Api&rs=1', 5000, 7, 3),
(51, 'Pizza muối ớt', 'https://th.bing.com/th/id/OIP.O3POkqht-eJ19PKVVXa6LQHaEo?pid=Api&rs=1', 15000, 6, 6),
(52, 'Pizza Tomato', 'https://www.weekendnotes.com/im/007/09/pizza-delicious-yummy-food-the-australian-brewery-1.jpg', 20000, 7, 6),
(53, 'Pizza muối cay', 'https://s-media-cache-ak0.pinimg.com/originals/81/a4/4d/81a44dea8e8703b48e1ae5241efc1772.jpg', 50000, 5, 6),
(54, 'Pizza cheese', 'https://data.1freewallpapers.com/download/pizza-delicious-food.jpg', 5000, 3, 6),
(55, 'Pizza thitk', 'https://data.1freewallpapers.com/download/pizza-delicious-food.jpg', 15000, 4, 6),
(56, 'Hamburger muối ớt', 'https://fr.chatelaine.com/wp-content/uploads/2019/06/HamburgerDecadent_768x1024.jpg', 15000, 6, 5),
(57, 'Hamburrger', 'https://www.vartesmajklem.cz/obrazky-large/americky-grilovany-hamburger_201902051153151616683359.jpg', 20000, 7, 5),
(58, 'Hamburger phomat', 'https://www.plainedefrance.fr/wp-content/uploads/2017/09/Hamburger.jpg', 50000, 5, 5),
(59, 'Hamburger muối', 'https://www.plainedefrance.fr/wp-content/uploads/2017/09/Hamburger.jpg', 5000, 3, 5),
(60, 'Khoai Tây chiên xù', 'https://dohyunkim183.files.wordpress.com/2013/10/kimbap.jpg', 15000, 6, 7),
(61, 'Khoai Tây chiên muối Xam', 'https://shameleonskitchen.com/wp-content/uploads/2019/02/kimbap1-3000x1953.jpg', 20000, 7, 7),
(62, 'Khoai tây Kem', 'https://s-media-cache-ak0.pinimg.com/originals/81/a4/4d/81a44dea8e8703b48e1ae5241efc1772.jpg', 5, 5, 7),
(63, 'Khoai tây chiên nhà làm', 'https://th.bing.com/th/id/OIP.nclHunNGOn4IlIodEncLXgAAAA?pid=Api&rs=1', 5000, 7, 7),
(64, 'Khoai tây chiên Coco', 'https://th.bing.com/th/id/OIP.nclHunNGOn4IlIodEncLXgAAAA?pid=Api&rs=1', 15000, 8, 7);

-- --------------------------------------------------------

--
-- Table structure for table `uudai_types`
--

CREATE TABLE `uudai_types` (
  `idtype` int(11) NOT NULL,
  `nametype` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uudai_types`
--

INSERT INTO `uudai_types` (`idtype`, `nametype`) VALUES
(1, 'MilkTea'),
(2, 'Mì cay'),
(3, 'Kim bap'),
(4, 'Kem'),
(5, 'Hamburger'),
(6, 'Pizza'),
(7, 'Khoai Tây chiên');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id_address`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cartID`);

--
-- Indexes for table `codeuudai`
--
ALTER TABLE `codeuudai`
  ADD PRIMARY KEY (`IdCode`);

--
-- Indexes for table `endows`
--
ALTER TABLE `endows`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idProducts`),
  ADD KEY `idTypes` (`idTypes`),
  ADD KEY `idShops` (`idShops`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`idShops`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`idTypes`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`);

--
-- Indexes for table `uudai_product`
--
ALTER TABLE `uudai_product`
  ADD PRIMARY KEY (`idsp`),
  ADD KEY `products` (`idType`);

--
-- Indexes for table `uudai_types`
--
ALTER TABLE `uudai_types`
  ADD PRIMARY KEY (`idtype`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id_address` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cartID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `codeuudai`
--
ALTER TABLE `codeuudai`
  MODIFY `IdCode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `uudai_product`
--
ALTER TABLE `uudai_product`
  MODIFY `idsp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `information`
--
ALTER TABLE `information`
  ADD CONSTRAINT `information_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`idUsers`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`idTypes`) REFERENCES `types` (`idTypes`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`idShops`) REFERENCES `shops` (`idShops`);

--
-- Constraints for table `uudai_product`
--
ALTER TABLE `uudai_product`
  ADD CONSTRAINT `products` FOREIGN KEY (`idType`) REFERENCES `uudai_types` (`idtype`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
