package com.example.grapfoodteam.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.DetailsSaleActivity;
import com.example.grapfoodteam.ExpandableHeightListView;
import com.example.grapfoodteam.R;
import com.example.grapfoodteam.adapter.SaleApdater;
import com.example.grapfoodteam.model.Sale;
import com.example.grapfoodteam.util.Connect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UuDaiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UuDaiFragment extends Fragment {
    TextView txtXemchitiet;
    ArrayList<Sale> arrSale;
    SaleApdater adapter;
    ExpandableHeightListView lv_sale;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public UuDaiFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UuDaiFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UuDaiFragment newInstance(String param1, String param2) {
        UuDaiFragment fragment = new UuDaiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_uu_dai, container, false);
        arrSale=new ArrayList<>();
        adapter=new SaleApdater(getContext(),arrSale,R.layout.sale_item);
        lv_sale=(ExpandableHeightListView) view.findViewById(R.id.listview_sale);
        lv_sale.setAdapter(adapter);
        lv_sale.setExpanded(true);
        getDataSale(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/sale/getData.php"));

        lv_sale.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myintent=new Intent(getContext(), DetailsSaleActivity.class);

                myintent.putExtra("image_sale",arrSale.get(i).getImageSale());
                myintent.putExtra("background_sale",arrSale.get(i).getBackgroundSale());
                myintent.putExtra("ten_sale",arrSale.get(i).getNameSale());
                myintent.putExtra("description_sale",arrSale.get(i).getDescriptionSale());
                myintent.putExtra("start_sale",arrSale.get(i).getDayStart());
                myintent.putExtra("finish_sale",arrSale.get(i).getDayFinish());
                startActivity(myintent);
            }
        });

        return view;
    }
    private  void getDataSale(String Url)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.GET, Url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                arrSale.clear();
                for(int i=0;i<response.length();i++)
                {
                    try {
                        JSONObject jsonObject=response.getJSONObject(i);
                        arrSale.add(new Sale(jsonObject.getInt("ID"),
                                jsonObject.getString("NameSale"),
                                jsonObject.getString("ImageSale"),
                                jsonObject.getString("BackSale"),
                                jsonObject.getString("DayStart"),
                                jsonObject.getString("DayFinish"),
                                jsonObject.getString("Description")
                        ));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
}