package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.model.address;
import com.example.grapfoodteam.util.Connect;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DiaChiActivity extends AppCompatActivity implements OnMapReadyCallback {
    GoogleMap map;
    LinearLayout linearLayout;
    EditText edtTen,edtGhiChu,edtChitietDiachi;
    LinearLayout btnLuuDiaChi;
    Intent myIntent;
    ImageView imgBack;
    ArrayList<address> arrAddress;
    TextView txt_diachi,txt_diachichitiet,txt_luudiachi;
    String address_details,note,addressname;
    int id_address=0;
    String urlDiachi= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/address/insert.php");
    String urlCapnhat=Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/address/update.php");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dia_chi);

        MapFragment mapFragment= (MapFragment) getFragmentManager().findFragmentById(R.id.mymap);
        mapFragment.getMapAsync(this);

        edtTen=(EditText) findViewById(R.id.EdittextTen);
        edtGhiChu=(EditText) findViewById(R.id.edittextGhiChu);
        edtChitietDiachi=(EditText) findViewById(R.id.edittextChitietdiachi);
        btnLuuDiaChi=(LinearLayout) findViewById(R.id.buttonLuuDiaChi);
        imgBack=(ImageView) findViewById(R.id.back);
        txt_diachi=(TextView) findViewById(R.id.text_diachi);
        txt_diachichitiet=(TextView) findViewById(R.id.text_diachichitiet);
        txt_luudiachi=(TextView) findViewById(R.id.text_luudiachi);
        //Lấy địa chỉ
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("diachihientaichitiet");
        if(bundle!=null)
        {

            String map=bundle.getString("diachihientai");
            String diachi=bundle.getString("diachi");
            address_details=bundle.getString("addressdetail");
            id_address=bundle.getInt("id_address");
            note=bundle.getString("note");
            addressname=bundle.getString("addressname");
            edtTen.setText(addressname);
            edtChitietDiachi.setText(address_details);
            txt_diachi.setText(diachi);
            txt_diachichitiet.setText(map);
            edtGhiChu.setText(note);
        }
        //

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DiaChiActivity.this,CardActivity.class);
                startActivity(intent);
            }
        });
        edtTen.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!edtTen.getText().toString().equals(""))
                {
                    btnLuuDiaChi.setBackground(getResources().getDrawable(R.drawable.botron3));
                    txt_luudiachi.setTextColor(getResources().getColor(R.color.white));
                }

            }
        });
        edtChitietDiachi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!edtChitietDiachi.getText().toString().equals(""))
                {
                    btnLuuDiaChi.setBackground(getResources().getDrawable(R.drawable.botron3));
                    txt_luudiachi.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });
        //Ghi chú
        btnLuuDiaChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(address_details.isEmpty())
                {
                    themDiachi(urlDiachi);
                }
                else
                {
                    CapnhatDiachi(urlCapnhat);
                }

            }
        });
        //Kết thúc Ghi chú


    }
    public void truyenDuLieu()
    {
        myIntent=new Intent(DiaChiActivity.this, CardActivity.class);
        String ghichu=edtGhiChu.getText().toString();
        String chitietdiachi=edtChitietDiachi.getText().toString();
        Bundle bundle=new Bundle();
        bundle.putString("ghichu",ghichu);
        bundle.putString("chitietdiachi",chitietdiachi);
        myIntent.putExtra("dulieu", bundle);
        //myIntent.putExtra("dulieu",arrayCourse);
        // myIntent.putExtra("dulieu",2017);

    }
    private  void themDiachi(String url)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("success"))
                {
                    Toast.makeText(DiaChiActivity.this,"Thêm thành công",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(DiaChiActivity.this,CardActivity.class));
                }else
                {
                    Toast.makeText(DiaChiActivity.this,"Lỗi thêm!!!",Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DiaChiActivity.this,"Xảy ra lỗi",Toast.LENGTH_LONG).show();
                Log.d("AAA","Lỗi!\n"+error.toString());
            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("address_name",edtTen.getText().toString().trim());
                params.put("address",txt_diachichitiet.getText().toString().trim());
                params.put("address_details",edtChitietDiachi.getText().toString().trim());
                params.put("note",edtGhiChu.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void CapnhatDiachi(String url)
    {
        final RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.trim().equals("success"))
                        {
                            Toast.makeText(DiaChiActivity.this,"Cập nhật thành công",Toast.LENGTH_LONG).show();
                            startActivity(new Intent(DiaChiActivity.this,CardActivity.class));
                        }else
                        {
                            Toast.makeText(DiaChiActivity.this,"Lỗi cập nhật",Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DiaChiActivity.this,"Xảy ra lỗi",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("id_address",String.valueOf(id_address));
                params.put("address_name",edtTen.getText().toString().trim());
                params.put("address",txt_diachichitiet.getText().toString().trim());
                params.put("address_details",edtChitietDiachi.getText().toString().trim());
                params.put("note",edtGhiChu.getText().toString().trim());

                return params;

            }
        };
        requestQueue.add(stringRequest);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        LatLng sydney=new LatLng(16.06056,108.19611);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,13));
        map.addMarker(new MarkerOptions()
                .title("Nhà riêng")
                .snippet("Hải Châu, Đà Nẵng")
                .position(sydney)
        );

    }
}