package com.example.grapfoodteam.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grapfoodteam.R;
import com.example.grapfoodteam.model.Shop;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ShopAdapter extends BaseAdapter {

    private Context context;
    private List<Shop> shop_data;
    private int layout;

    public ShopAdapter(Context context, List<Shop> shop_data, int layout) {
        this.context = context;
        this.shop_data = shop_data;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return shop_data.size();
    }

    @Override
    public Object getItem(int i) {
        return shop_data.get(i);
    }

    @Override
    public long getItemId(int i) {return shop_data.get(i).getId();}
    private class ViewHolder{
        ImageView picturelist;
        TextView TenQuan,Address,theloai,khuyenmai,thoigian,km;
    }
    @Override
    public android.view.View getView(int i, android.view.View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.TenQuan=(TextView) view.findViewById(R.id.name_shop);
            holder.Address=(TextView) view.findViewById(R.id.type_shop);
            holder.picturelist=(ImageView) view.findViewById(R.id.image_shop);
            holder.theloai=(TextView) view.findViewById(R.id.type_shop);
            holder.khuyenmai=(TextView) view.findViewById(R.id.km);
            holder.thoigian=(TextView) view.findViewById(R.id.time);
            holder.km=(TextView) view.findViewById(R.id.kilo);

            view.setTag(holder);
        }
        else{
            holder= (ViewHolder) view.getTag();
        }

        Shop shops= shop_data.get(i);
        holder.TenQuan.setText(shops.getTenquan());
        Picasso.get().load(shops.getHinh())
                .into(holder.picturelist);
        holder.Address.setText(shops.getAddress());
        holder.theloai.setText(shops.getTheloai());
        holder.khuyenmai.setText(shops.getKhuyenmai());
        holder.thoigian.setText(shops.getThoigian());
        holder.km.setText(shops.getKm());
        return view;
    };




}
