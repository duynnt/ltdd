package com.example.grapfoodteam.model;

public class CodeUuDai {
    private int id;
    private String maCode;

    public CodeUuDai(int id, String maCode) {
        this.id = id;
        this.maCode = maCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaCode() {
        return maCode;
    }

    public void setMaCode(String maCode) {
        this.maCode = maCode;
    }
}
