package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class ThemNhaActivity extends AppCompatActivity {
    ImageView imgClose,imgGooglemap;
    Intent myIntent;
    EditText edtDiaChiNha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_nha);
        Anhxa();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                truyenDuLieu();
                startActivity(myIntent);
            }
        });
        imgGooglemap=(ImageView) findViewById(R.id.imageGoogleMap);
        imgGooglemap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ThemNhaActivity.this,MapActivity.class));
            }
        });
    }
    public void truyenDuLieu()
    {
        myIntent=new Intent(ThemNhaActivity.this, ChiTietDiaChiActivity.class);
        String location=edtDiaChiNha.getText().toString();
        Bundle bundle=new Bundle();
        bundle.putString("diachi",location);
        myIntent.putExtra("dulieuDiaChi", bundle);
        //myIntent.putExtra("dulieu",arrayCourse);
        // myIntent.putExtra("dulieu",2017);

    }
    private void Anhxa()
    {
        edtDiaChiNha=(EditText) findViewById(R.id.editTextDiaChiNha);
        imgClose=(ImageView) findViewById(R.id.imageClose);
    }

}