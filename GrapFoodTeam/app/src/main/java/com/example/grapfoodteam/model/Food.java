package com.example.grapfoodteam.model;

public class Food {
    private int id;
    private String Ten;
    private int gia;
    private String Hinh;
    private int idShop;

    public Food(int id, String ten, int gia, String hinh, int idShop) {
        this.id = id;
        Ten = ten;
        this.gia = gia;
        Hinh = hinh;
        this.idShop = idShop;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public int getGia() {
        return gia;
    }

    public void setGia(int gia) {
        this.gia = gia;
    }

    public String getHinh() {
        return Hinh;
    }

    public void setHinh(String hinh) {
        Hinh = hinh;
    }

    public int getIdShop() {
        return idShop;
    }

    public void setIdShop(int idShop) {
        this.idShop = idShop;
    }
}
