package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.adapter.ProductListAdapter;
import com.example.grapfoodteam.model.ProductGridDetail;
import com.example.grapfoodteam.model.ProductList;
import com.example.grapfoodteam.model.profilefood;
import com.example.grapfoodteam.util.Connect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShowAll extends AppCompatActivity {
    ListView listView;
    ArrayList<ProductList> arrayList;
    ProductListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all);
        Toolbar toolbar=findViewById(R.id.toolbar_showall);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.back);
        mapping();
        getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shops.php"));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int idd =arrayList.get(position).getId();
                Intent intent=new Intent(ShowAll .this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",idd);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        });
    }
    private void mapping(){
        listView=(ListView) findViewById(R.id.listviewShowAll);
        arrayList=new ArrayList<>();
        adapter=new ProductListAdapter(com.example.grapfoodteam.ShowAll.this,arrayList,R.layout.line_product_verticol);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void getData(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(ShowAll.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                arrayList.add(new ProductList(object.getInt("id"),
                                        object.getString("image"),
                                        object.getString("name"),
                                        object.getString("note"),
                                        "4.5",
                                        "15 phút",
                                        "2 km",
                                        "Nhập Pepsifreeship...",
                                        "3 địa điểm gần bạn .."));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ShowAll.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
}