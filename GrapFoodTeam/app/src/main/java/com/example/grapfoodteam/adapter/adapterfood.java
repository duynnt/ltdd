package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grapfoodteam.R;
import com.example.grapfoodteam.model.profilefood;
import com.squareup.picasso.Picasso;

import java.util.List;

public class adapterfood extends BaseAdapter {
    private Context context;
    private int layout;
    private List<profilefood> profilefoodList;

    public adapterfood(Context context, int layout, List<profilefood> profilefoodList) {
        this.context = context;
        this.layout = layout;
        this.profilefoodList = profilefoodList;
    }

    @Override
    public int getCount() {
        return profilefoodList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class viewholder{
        ImageView imghinh;
        TextView txttenquan, txttheloai, txtpoint, txttime, txtklo, txtkm;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewholder holder;
    if (convertView == null){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView =inflater.inflate(layout,null);
    //anh xa
        holder = new viewholder();
        holder.txttenquan = (TextView) convertView.findViewById(R.id.txttenpr);
        holder.txttheloai = (TextView) convertView.findViewById(R.id.txttheloaipr);
        holder.txtpoint = (TextView) convertView.findViewById(R.id.txtpointpr);
        holder.txttime = (TextView) convertView.findViewById(R.id.txttimepr);
        holder.txtklo = (TextView) convertView.findViewById(R.id.txtklo);
        holder.txtkm = (TextView) convertView.findViewById(R.id.txtkm);
        holder.imghinh = (ImageView) convertView.findViewById(R.id.imghinhpr);
        convertView.setTag(holder);
    }else {
        holder =(viewholder)convertView.getTag();
    }
    //set data
    profilefood pr = profilefoodList.get(position);
    holder.txttenquan.setText(pr.getTenquan());
    holder.txttheloai.setText(pr.getTheloai());
    holder.txtpoint.setText(pr.getDanhgia());
    holder.txttime.setText(pr.getThoigian());
    holder.txtklo.setText(pr.getKm());
    holder.txtkm.setText(pr.getKhuyenmai());
        Picasso.get().load(pr.getHinh())
                .into(holder.imghinh);
    return convertView;
    }
}
