package com.example.grapfoodteam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.grapfoodteam.adapter.ProductAdapter;
import com.example.grapfoodteam.adapter.ProductListAdapter;
import com.example.grapfoodteam.model.Product;
import com.example.grapfoodteam.model.ProductList;



import com.example.grapfoodteam.model.Shop;
import com.example.grapfoodteam.util.Connect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrangChuActivity extends AppCompatActivity {
    LinearLayout lngantoi,lnkm, lncom, lnanvat, lnquanmoi, lnts, lnfastfodd, lntatca;;
    ViewFlipper viewFlipper;
    Button bntuudai;
    //thao
    RecyclerView recyclerView1,recyclerView2,recyclerView3;
    List<Product> list1,list2,list3;
    ExpandableHeightListView listView1;
    ArrayList<ProductList> arrayList1;
    ProductListAdapter adapterlist1;
    TextView txtShowAll1,txtShowAll2,txtShowAll3;
    private ProductAdapter.RecyclerViewClickListener listener,listener1,listener2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_chu);
        anhxa();
        LinearLayout layoutSearch = (LinearLayout) findViewById(R.id.lnsearch) ;
        layoutSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrangChuActivity.this, Search.class);
                startActivity(intent);
            }
        });
        viewFlipper = (ViewFlipper) findViewById(R.id.viewvf);
        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bntuudai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this, TrangchuActivity1.class);
                startActivity(intent);
            }
        });
        lngantoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lnkm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lncom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lnfastfodd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lnquanmoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lnanvat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lnanvat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        lnts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrangChuActivity.this,DetailGanToiActivity.class);
                startActivity(intent);
            }
        });
        //thao
        mapping1();
        mapping2();
        mapping3();
        mappinglist1();
        txtShowAll1=(TextView) findViewById(R.id.txtShowAll1);
        txtShowAll2=(TextView) findViewById(R.id.txtShowAll2);
        txtShowAll3=(TextView) findViewById(R.id.txtShowAll3);
        txtShowAll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TrangChuActivity.this,ShowAll.class));
            }
        });
        txtShowAll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TrangChuActivity.this,ShowAll.class));
            }
        });
        txtShowAll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TrangChuActivity.this,ShowAll.class));
            }
        });
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int id=arrayList1.get(i).getId();
                Intent intent=new Intent(TrangChuActivity.this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.go_yeuthich:
                Intent intent = new Intent(TrangChuActivity.this, Yeuthich.class);
                startActivity(intent);
                break;
            case R.id.canhan:
                startActivity(new Intent(TrangChuActivity.this,profile.class));
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    private void anhxa(){
        bntuudai= (Button) findViewById(R.id.uudaiview);
        lngantoi = (LinearLayout) findViewById(R.id.lngantoi);
        lnkm = (LinearLayout) findViewById(R.id.lnkm);
        lncom = (LinearLayout) findViewById(R.id.lncom);
        lnanvat = (LinearLayout) findViewById(R.id.lnanvat);
        lnquanmoi = (LinearLayout) findViewById(R.id.lnquanmoi);
        lnts = (LinearLayout) findViewById(R.id.lnts);
        lnfastfodd = (LinearLayout) findViewById(R.id.lnfastfood);
        lntatca = (LinearLayout) findViewById(R.id.lnallquan);
    }
    private  void mappinglist1(){
        listView1=(ExpandableHeightListView) findViewById(R.id.listview1);
        arrayList1=new ArrayList<>();
        adapterlist1=new ProductListAdapter(TrangChuActivity.this,arrayList1,R.layout.line_product_verticol);
        listView1.setAdapter(adapterlist1);
        listView1.setExpanded(true);
        String url= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shoplimit.php");
        GetDuLieuList(url,"9","10");
    }
    private void mapping1(){
        recyclerView1=(RecyclerView) findViewById(R.id.recyclerView1);
        list1=new ArrayList<>();
        String url= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shoplimit.php");
        setOnclickListener();
        LinearLayoutManager layoutManager1=new LinearLayoutManager(this);
        layoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView1.setLayoutManager(layoutManager1);
        ProductAdapter adapter1=new ProductAdapter(this,list1,listener);
        recyclerView1.setAdapter(adapter1);
        GetDuLieu(url,"0","3",list1,adapter1);
    }

    private void setOnclickListener() {
        listener=new ProductAdapter.RecyclerViewClickListener() {
            @Override
            public void OnClick(View v, int position) {
                int id=list1.get(position).getId();
                Intent intent=new Intent(TrangChuActivity.this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        };
    }
    private void setOnclickListener1() {
        listener=new ProductAdapter.RecyclerViewClickListener() {
            @Override
            public void OnClick(View v, int position) {
                int id=list2.get(position).getId();
                Intent intent=new Intent(TrangChuActivity.this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        };
    }
    private void setOnclickListener2() {
        listener=new ProductAdapter.RecyclerViewClickListener() {
            @Override
            public void OnClick(View v, int position) {
                int id=list3.get(position).getId();
                Intent intent=new Intent(TrangChuActivity.this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        };
    }


    private void mapping2(){
        String url= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shoplimit.php");
        recyclerView2=(RecyclerView) findViewById(R.id.recyclerView2);
        list2=new ArrayList<>();
        setOnclickListener1();
        LinearLayoutManager layoutManager2=new LinearLayoutManager(this);
        layoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView2.setLayoutManager(layoutManager2);
        ProductAdapter adapter2=new ProductAdapter(this,list2,listener);
        recyclerView2.setAdapter(adapter2);
        GetDuLieu(url,"3","3",list2,adapter2);
    }
    private void mapping3(){
        recyclerView3=(RecyclerView) findViewById(R.id.recyclerView3);
        list3=new ArrayList<>();
        String url= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shoplimit.php");
        setOnclickListener2();
        LinearLayoutManager layoutManager3=new LinearLayoutManager(this);
        layoutManager3.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView3.setLayoutManager(layoutManager3);
        ProductAdapter adapter3=new ProductAdapter(this,list3,listener);
        recyclerView3.setAdapter(adapter3);
        GetDuLieu(url,"6","3",list3,adapter3);
    }
    private void GetDuLieu(String url, final String start, final String end, final List<Product> array, final ProductAdapter adapter){
        RequestQueue requestQueue= Volley.newRequestQueue(TrangChuActivity.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    array.add(new Product(jsonObject.getInt("id"),
                                        "Khuyến mãi",
                                        jsonObject.getString("image"),
                                        "3,6 km",
                                        jsonObject.getString("name"),
                                        "30%"));
                                }
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TrangChuActivity.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("start",start);
                prams.put("end",end);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void GetDuLieuList(String url, final String start, final String end){
        RequestQueue requestQueue= Volley.newRequestQueue(TrangChuActivity.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    arrayList1.add(new ProductList(jsonObject.getInt("id"),
                                            jsonObject.getString("image"),
                                            jsonObject.getString("name"),
                                            jsonObject.getString("note"),
                                            "4.5",
                                            "15 phút",
                                            "2 km",
                                            "Nhập Pepsifreeship...",
                                            "3 địa điểm gần bạn .."));
                                }
                                adapterlist1.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TrangChuActivity.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("start",start);
                prams.put("end",end);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
}