package com.example.grapfoodteam;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.adapter.CardApdater;
import com.example.grapfoodteam.adapter.MyAdapter;
import com.example.grapfoodteam.model.Card;
import com.example.grapfoodteam.model.SanPham;
import com.example.grapfoodteam.model.address;
import com.example.grapfoodteam.util.Connect;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CardActivity extends AppCompatActivity {
    RecyclerView rycSanPham;
    ArrayList<SanPham> arraySanpham;
    ArrayList<Card> arrCart;
    ArrayList<address> arrAddress;
    MyAdapter adapter;
    CardApdater cardApdater;
    LinearLayout ln;
    Intent myIntent;
    Switch sw;
    ImageView imgMoney,imgClose,imgCalendar,imgClock,imgDiaChi,imgBack;
    TextView txtMoney,txtThaydoi,txtCalendar,txtHomnay,txtClock1,txtClock2,txtSuaDiaChi,txtGhichu
            ,txtchitietdiachi,txtSwitch1,txtSwitch2,txtSwitch3,txtDiachi,txtMap,txt_card_tongtien,
            txt_tamtinh,txt_phidichvu;
    Button btnDoiPhuongThucThanhToan;
    LinearLayout btnUudai,btnDatdon;
    FusedLocationProviderClient fusedLocationProviderClient;
    ExpandableHeightListView lv_card;
    int number=0,price=0,sumMoney=0,tamtinh=0,tienmonan=0;
    private final static int LOCATION_REQUEST_CODE = 23;
    String urlAddress= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/address/getdata.php");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        Anhxa();

        LinearLayoutManager manager1=new LinearLayoutManager(this);
        manager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        rycSanPham.setLayoutManager(manager1);
        adapter=new MyAdapter(this,arraySanpham);
        rycSanPham.setAdapter(adapter);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CardActivity.this,TrangChuActivity.class));
            }
        });


        imgDiaChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                truyenDuLieu();
                startActivity(myIntent);
            }
        });
        //switch
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                txtSwitch1.setText((sw.isChecked() ? "Lấy dụng cụ ăn uống nhựa" :"Không dụng cụ ăn uống nhựa "));
                txtSwitch2.setText("Chúng tôi sẽ gửi quán yêu của bạn");
                txtSwitch3.setText((sw.isChecked() ? "Hãy chung tay bảo vệ môi trường vào lấn tới nhé":" Cảm ơn bạn đã giúp giảm chất thải nhựa" ));
            }
        });


        //ListviewCart
        cardApdater=new CardApdater(this,arrCart,R.layout.card_item);
        lv_card.setAdapter(cardApdater);
        lv_card.setExpanded(true);
        GetData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/cart/getdata.php"));
        //
        //Lấy dữ liệu 2
        Intent intent2=getIntent();
        Bundle bundle2=intent2.getBundleExtra("dataDiaChi");
        if(bundle2!=null)
        {

            String diachi=bundle2.getString("address");
            int diemDung=diachi.indexOf(",");
            String address=diachi.substring(0,diemDung);
            txtMap.setText(diachi);
            txtDiachi.setText(address);
        }else
        {
            //get curent location
            fusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
            if(ActivityCompat.checkSelfPermission(CardActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
            {
                fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location=task.getResult();
                        if(location!=null)
                        {
                            Geocoder geocoder=new Geocoder(CardActivity.this, Locale.getDefault());
                            try {
                                List<Address> addressList=geocoder.getFromLocation(location.getLatitude(),
                                        location.getLongitude(),1);

                                txtMap.setText(addressList.get(0).getAddressLine(0));
                                String address_current=addressList.get(0).getAddressLine(0);
                                int diemDung=address_current.indexOf(",");
                                String location_current=address_current.substring(0,diemDung);
                                txtDiachi.setText(location_current);
                            }catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            Toast.makeText(CardActivity.this,"Location null error",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }else
            {
                ActivityCompat.requestPermissions(CardActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);
            }
        }
        //Kết thúc lấy dữ liệu
        //Láy dữ liệu
        GetAddress(urlAddress);


        /*Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("dulieu");
        if(bundle!=null)
        {

            String ghichu=bundle.getString("ghichu");
            String chitietdiachi=bundle.getString("chitietdiachi");
            if(ghichu.equals("")&&chitietdiachi.equals(""))
            {
                txtchitietdiachi.setText("Chưa thêm chi tiết địa chỉ");
                txtGhichu.setText("Chưa thêm ghi chú cho tài xế ");
            }
            else
            {
                txtGhichu.setText(ghichu);
                txtchitietdiachi.setText(chitietdiachi);

            }

        }*/

        //Kết thúc lấy dữ liệu


        txtSuaDiaChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                truyenDuLieu_diachi();
            }
        });
        txtThaydoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogSheetview= LayoutInflater.from(CardActivity.this).inflate(R.layout.dialog_thoigian,null);
                final BottomSheetDialog dialogThoiGian=new BottomSheetDialog(CardActivity.this);
                dialogThoiGian.setContentView(dialogSheetview);
                imgClose=(ImageView) dialogSheetview.findViewById(R.id.imageClose);
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogThoiGian.dismiss();
                    }
                });
                //Calendar
                txtCalendar=(TextView) dialogSheetview.findViewById(R.id.textviewCalendar);
                imgCalendar=(ImageView) dialogSheetview.findViewById(R.id.ImageCalendar);
                txtHomnay=(TextView) dialogSheetview.findViewById(R.id.homnay);
                RelativeLayout relativeLayoutChonNgay=(RelativeLayout) dialogSheetview.findViewById(R.id.Chonngay);
                relativeLayoutChonNgay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chonNgay();
                    }
                });

                //Kết thúc Calendar

                //Clock
                imgClock=(ImageView) dialogSheetview.findViewById(R.id.imageClock);
                txtClock1=(TextView) dialogSheetview.findViewById(R.id.textviewClock1);
                txtClock2=(TextView) dialogSheetview.findViewById(R.id.textviewClock2);
                /*
                Calendar calendar=Calendar.getInstance();
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("HH:mm");
                txtClock1.setText(simpleDateFormat.format(calendar.getTime()));*/
                RelativeLayout relativeLayoutChonGio=(RelativeLayout) dialogSheetview.findViewById(R.id.Chongio);
                relativeLayoutChonGio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chonGio();
                    }
                });
                //Kết thúc Clock
                dialogThoiGian.show();
            }
        });
        btnDatdon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogSheetview= LayoutInflater.from(CardActivity.this).inflate(R.layout.dialog_thanhtoan,null);
                BottomSheetDialog dialog=new BottomSheetDialog(CardActivity.this);
                dialog.setContentView(dialogSheetview);
                btnDoiPhuongThucThanhToan=(Button) dialogSheetview.findViewById(R.id.buttonDoiPhuongThucThanhToan);
                btnDoiPhuongThucThanhToan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(CardActivity.this,MoneyActivity.class);
                        startActivity(intent);
                    }
                });
                dialog.show();
                //btnUudai=(Button) dialogSheetview.findViewById(R.id.buttonUudai);
            }
        });
        imgMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CardActivity.this,MoneyActivity.class);
                startActivity(intent);
            }
        });
        txtMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CardActivity.this,MoneyActivity.class);
                startActivity(intent);
            }
        });
        btnUudai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CardActivity.this,EndowActivity.class);
                startActivity(intent);
            }
        });

    }
    private  void GetData(String Url)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.GET, Url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                arrCart.clear();
                for(int i=0;i<response.length();i++)
                {
                    try {
                        JSONObject jsonObject=response.getJSONObject(i);
                        arrCart.add(new Card(jsonObject.getInt("Cart_ID"),
                                jsonObject.getInt("Product_ID"),
                                jsonObject.getString("Ten"),
                                jsonObject.getString("Hinh"),
                                jsonObject.getInt("SoLuong"),
                                jsonObject.getInt("Gia"),
                                jsonObject.getString("Note")
                        ));
                        for(int j=i;j<arrCart.size();j++)
                        {
                            number=arrCart.get(i).getQty();
                            price=arrCart.get(i).getPrice();
                            tienmonan=number*price;
                            tamtinh+=tienmonan;
                        }
                        DecimalFormat format=new DecimalFormat("###,###.###");
                        txt_tamtinh.setText(format.format(tamtinh)+"đ");
                        sumMoney=tamtinh+19000;
                        txt_card_tongtien.setText(format.format(sumMoney)+"đ");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                cardApdater.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CardActivity.this,error.toString(),Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private  void GetAddress(String Url)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.GET, Url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //arrCart.clear();
                for(int i=0;i<response.length();i++)
                {
                    try {
                        JSONObject jsonObject=response.getJSONObject(i);
                        arrAddress.add(new address(jsonObject.getInt("ID_Address"),
                                jsonObject.getString("AddressName"),
                                jsonObject.getString("Address"),
                                jsonObject.getString("Address_Details"),
                                jsonObject.getString("Note")
                        ));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(arrAddress.size()>0)
                {
                    String ghichu=arrAddress.get(0).getAddressNote();
                    String chitietdiachi=arrAddress.get(0).getAddressDetails();
                    if(ghichu.equals("")&&chitietdiachi.equals(""))
                    {
                        txtchitietdiachi.setText("Chưa thêm chi tiết địa chỉ");
                        txtGhichu.setText("Chưa thêm ghi chú cho tài xế ");
                    }
                    else
                    {
                        txtGhichu.setText(ghichu);
                        txtchitietdiachi.setText(chitietdiachi);
                    }
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CardActivity.this,error.toString(),Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

    public void truyenDuLieu_diachi()
    {
        myIntent=new Intent(CardActivity.this, DiaChiActivity.class);
        String map=txtMap.getText().toString();
        String diachi=txtDiachi.getText().toString();
        int id=arrAddress.get(0).getId();
        String note=arrAddress.get(0).getAddressNote();
        String addressdetail=arrAddress.get(0).getAddressDetails();
        String addressname=arrAddress.get(0).getAddressName();
        Bundle bundle=new Bundle();
        bundle.putInt("id_address",id);
        bundle.putString("diachihientai",map);
        bundle.putString("diachi",diachi);
        bundle.putString("addressdetail",addressdetail);
        bundle.putString("note",note);
        bundle.putString("addressname",addressname);
        myIntent.putExtra("diachihientaichitiet", bundle);
        startActivity(myIntent);

    }
    public void truyenDuLieu()
    {
        myIntent=new Intent(CardActivity.this, ChiTietDiaChiActivity.class);
        String location=txtDiachi.getText().toString();
        Bundle bundle=new Bundle();
        bundle.putString("address",location);
        myIntent.putExtra("dataDiaChi", bundle);
        //myIntent.putExtra("dulieu",arrayCourse);
        // myIntent.putExtra("dulieu",2017);

    }
    private  void  chonGio()
    {
        final Calendar calendar=Calendar.getInstance();
        int gio=calendar.get(Calendar.HOUR_OF_DAY);
        int phut=calendar.get(Calendar.MINUTE);
        int phut2=phut+15;
        TimePickerDialog timePickerDialog=new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        // SimpleDateFormat simpleDateFormat=new SimpleDateFormat("HH:mm");
                        //calendar.set(0,0,0,i,i1);
                        //txtClock1.setText(simpleDateFormat.format(calendar.getTime()));
                        txtClock1.setText(i+":"+i1);
                        txtClock2.setText(i+":"+Integer.parseInt(String.valueOf(i1))+15);
                    }
                },gio,phut,android.text.format.DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
    private  void chonNgay()
    {
        final Calendar calendar=Calendar.getInstance();
        int ngay=calendar.get(Calendar.DATE);
        int thang=calendar.get(Calendar.MONTH);
        int nam=calendar.get(Calendar.YEAR);
        int thang2=thang+1;
        DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                //i: năm - i1:tháng - i2:ngày
                calendar.set(i,i1,i2);
               /* SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                txtCalendar.setText(simpleDateFormat.format(calendar.getTime()));
                txtHomnay.setText(simpleDateFormat.format(calendar.getTime()));*/
                txtCalendar.setText(i2+" thg "+i1);
                txtHomnay.setText(i2+" thg "+i1);
            }
        }, nam,thang2,ngay);
        datePickerDialog.show();
    }
    private  void Anhxa()
    {
        imgBack=(ImageView)findViewById(R.id.imgrevent);
        rycSanPham=(RecyclerView) findViewById(R.id.recyclerView);
        arraySanpham=new ArrayList<>();
        arrCart=new ArrayList<>();
        arrAddress=new ArrayList<>();
        imgMoney=(ImageView) findViewById(R.id.imageviewMoney);
        txtMoney=(TextView) findViewById(R.id.textviewMoney);
        btnUudai=(LinearLayout) findViewById(R.id.buttonUudai);
        btnDatdon =(LinearLayout) findViewById(R.id.buttonDatdon);
        txtThaydoi=(TextView) findViewById(R.id.textviewThaydoi);
        txtSuaDiaChi=(TextView) findViewById(R.id.suadiachi);
        txtGhichu=(TextView) findViewById(R.id.textviewGhichu);
        txtchitietdiachi=(TextView) findViewById(R.id.textviewchitietdiachi);
        txtSwitch1=(TextView) findViewById(R.id.switch_textview1);
        txtSwitch2=(TextView) findViewById(R.id.switch_textview2);
        txtSwitch3=(TextView) findViewById(R.id.switch_textview3);
        sw=(Switch) findViewById(R.id.switch_id);
        imgDiaChi=(ImageView) findViewById(R.id.imageDiaChi);
        txtDiachi=(TextView) findViewById(R.id.diachi);
        txtMap =(TextView) findViewById(R.id.diachichitiet);
        lv_card=(ExpandableHeightListView) findViewById(R.id.list_card);
        txt_card_tongtien=(TextView) findViewById(R.id.text_card_tongtien);
        txt_tamtinh=(TextView) findViewById(R.id.text_tamtinh);
        txt_phidichvu=(TextView)findViewById(R.id.text_phidichvu);
        arraySanpham.add(new SanPham("Pizza phô mai-size nhỏ 15cm","50.000",R.drawable.pizza1));
        arraySanpham.add(new SanPham("Mỳ ý thịt bò băm sốt cà chua","52.000",R.drawable.myy2));
        arraySanpham.add(new SanPham("Mỳ ý hải sản sốt kem phô mai đút lò nướng","50.000",R.drawable.myy2));
        arraySanpham.add(new SanPham("Pizza hải sản-size nhỏ 15cm","50.000",R.drawable.pizza2));
        arraySanpham.add(new SanPham("BBQ Pizza-Pizza thitk bò nướng size L 22cm","80.000",R.drawable.pizza3));
        arraySanpham.add(new SanPham("Mỳ ý thịt xông khói sốt kem phô mai đút lò nướng","70.000",R.drawable.myy3));
    }
}