package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.util.Connect;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class UpdateCartActivity extends AppCompatActivity {
    TextView txt_chitiet_ten,txt_chitiet_gia,txt_soluong,txt_chitiet_tongtien,txt_capnhatgiohang;
    ImageView img_chitiet_hinh,img_up_close;
    EditText edt_chitiet_luuy;
    RelativeLayout rePlus,reMinus;
    int gia=0,tongtien=0,cartid=0,soluongmonan=0;
    LinearLayout ln_capnhat;
    String urlupdate= Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/cart/update.php");
    String urlDelete=Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/cart/delete.php");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_cart);
        Anhxa();
        Bundle bundle=getIntent().getExtras();

        gia=bundle.getInt("gia");
        final int soluong=bundle.getInt("soluong");
        String linkhinh=bundle.getString("hinh");
        cartid=bundle.getInt("cartid");
        txt_chitiet_ten.setText(bundle.getString("ten"));
        DecimalFormat format=new DecimalFormat("###,###.###");
        txt_chitiet_gia.setText(format.format(gia));
        txt_soluong.setText(soluong+"");
        Picasso.get().load(linkhinh)
                .placeholder(R.drawable.pizza1)
                .error(R.drawable.pizza3)
                .into(img_chitiet_hinh);
        tongtien=soluong*gia;
        txt_chitiet_tongtien.setText(format.format(tongtien));
        soluongmonan=Integer.parseInt(txt_soluong.getText().toString());
        rePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln_capnhat.setBackground(getResources().getDrawable(R.drawable.botron));
                txt_capnhatgiohang.setVisibility(View.VISIBLE);
                soluongmonan++;
                txt_soluong.setText( soluongmonan+"");
                tongtien=tongtien+gia;
                DecimalFormat format=new DecimalFormat("###,###.###");
                txt_chitiet_tongtien.setText(format.format(tongtien));
                if(soluongmonan==0 )
                {
                    ln_capnhat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           // Toast.makeText(UpdateCartActivity.this,"Xóa",Toast.LENGTH_LONG).show();
                            //Toast.makeText(UpdateCartActivity.this,txt_soluong.getText().toString()+"",Toast.LENGTH_LONG).show();
                            deleteCart(cartid);
                        }
                    });
                }else
                {
                    ln_capnhat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Toast.makeText(UpdateCartActivity.this,"Cập nhật",Toast.LENGTH_LONG).show();
                            //Toast.makeText(UpdateCartActivity.this,txt_soluong.getText().toString()+"",Toast.LENGTH_LONG).show();
                            UpdateCart(urlupdate);
                        }
                    });
                }
            }
        });
        reMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(soluongmonan>0)
                {
                    soluongmonan--;
                    txt_soluong.setText(soluongmonan+"");
                    tongtien=tongtien-gia;
                    DecimalFormat format=new DecimalFormat("###,###.###");
                    txt_chitiet_tongtien.setText(format.format(tongtien));
                }
                if (soluongmonan==0 )
                {
                    txt_capnhatgiohang.setVisibility(View.GONE);
                    txt_chitiet_tongtien.setText("Xóa");
                    ln_capnhat.setBackground(getResources().getDrawable(R.drawable.botron11));
                    txt_chitiet_tongtien.setTextColor(getResources().getColor(R.color.white));
                    ln_capnhat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Toast.makeText(UpdateCartActivity.this,"Xóa",Toast.LENGTH_LONG).show();
                            //Toast.makeText(UpdateCartActivity.this,txt_soluong.getText().toString()+"",Toast.LENGTH_LONG).show();
                             deleteCart(cartid);
                        }
                    });
                }
            }
        });

        if(soluongmonan==0 )
        {
            ln_capnhat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(UpdateCartActivity.this,"Xóa",Toast.LENGTH_LONG).show();
                    //Toast.makeText(UpdateCartActivity.this,txt_soluong.getText().toString()+"",Toast.LENGTH_LONG).show();
                     deleteCart(cartid);
                }
            });
        }else
        {
            ln_capnhat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(UpdateCartActivity.this,"Cập nhật",Toast.LENGTH_LONG).show();
                    //Toast.makeText(UpdateCartActivity.this,txt_soluong.getText().toString()+"",Toast.LENGTH_LONG).show();
                    UpdateCart(urlupdate);
                }
            });
        }
        img_up_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(com.example.grapfoodteam.UpdateCartActivity.this,CardActivity.class));
            }
        });
    }
    private void UpdateCart(String url)
    {
        final RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.trim().equals("success"))
                        {
                            Toast.makeText(com.example.grapfoodteam.UpdateCartActivity.this,"Cập nhật thành công",Toast.LENGTH_LONG).show();
                            startActivity(new Intent(com.example.grapfoodteam.UpdateCartActivity.this,CardActivity.class));
                        }else
                        {
                            Toast.makeText(com.example.grapfoodteam.UpdateCartActivity.this,"Lỗi cập nhật",Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(com.example.grapfoodteam.UpdateCartActivity.this,"Xảy ra lỗi",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("cartid",String.valueOf(cartid));
                params.put("qty",txt_soluong.getText().toString().trim());
                params.put("price",String.valueOf(gia));
                params.put("note",edt_chitiet_luuy.getText().toString().trim());

                return params;

            }
        };
        requestQueue.add(stringRequest);
    }
    public void deleteCart(final int idcart)
    {
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, urlDelete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.trim().equals("success"))
                        {

                            //GetData("http://192.168.1.5:8080/cart/cart/getdata.php");
                            startActivity(new Intent(com.example.grapfoodteam.UpdateCartActivity.this,CardActivity.class));
                        }else
                        {
                            Toast.makeText(com.example.grapfoodteam.UpdateCartActivity.this,"Lỗi Xóa!!!",Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(com.example.grapfoodteam.UpdateCartActivity.this,"Xảy ra lỗi",Toast.LENGTH_LONG).show();
                        Log.d("AAA","Lỗi!\n"+error.toString());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cartID",String.valueOf(idcart));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void Anhxa()
    {
        txt_chitiet_gia=(TextView) findViewById(R.id.text_chitiet_gia);
        txt_chitiet_ten=(TextView) findViewById(R.id.text_chitiet_ten);
        txt_soluong=(TextView) findViewById(R.id.text_chitiet_soluong);
        txt_chitiet_tongtien=(TextView) findViewById(R.id.text_chitiet_tongtien);
        edt_chitiet_luuy=(EditText) findViewById(R.id.edit_chitiet_luuy);
        img_chitiet_hinh=(ImageView) findViewById(R.id.image_chitiet_hinh);
        rePlus=(RelativeLayout) findViewById(R.id.relative_plus);
        reMinus=(RelativeLayout) findViewById(R.id.relative_minus);
        ln_capnhat=(LinearLayout) findViewById(R.id.linear_capnhat);
        txt_capnhatgiohang=(TextView) findViewById(R.id.text_capnhatgiohang);
        img_up_close=(ImageView) findViewById(R.id.image_up_cart_close);
    }
}