package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grapfoodteam.model.ProductList;
import com.example.grapfoodteam.R;
import com.squareup.picasso.Picasso;


import java.util.List;

public class ProductListAdapter extends BaseAdapter {
    private Context context;
    private List<ProductList> lists;
    private int layout;

    public ProductListAdapter(Context context, List<ProductList> lists, int layout) {
        this.context = context;
        this.lists = lists;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolder{
        ImageView picturelist;
        TextView namelist,notelist,starlist,timelist,rangelist,salelist,addressnearlist;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holder=new ViewHolder();
            holder.picturelist=(ImageView) view.findViewById(R.id.image_picturelist);
            holder.namelist=(TextView) view.findViewById(R.id.txtnamelist);
            holder.notelist=(TextView) view.findViewById(R.id.txtnotelist);
            holder.starlist=(TextView) view.findViewById(R.id.txtstarlist);
            holder.timelist=(TextView) view.findViewById(R.id.txttimelist);
            holder.rangelist=(TextView) view.findViewById(R.id.txtrangelist);
            holder.salelist=(TextView) view.findViewById(R.id.txtsalelist);
            holder.addressnearlist=(TextView) view.findViewById(R.id.txtaddressnearlist);
            view.setTag(holder);
        }
        else
            holder= (ViewHolder) view.getTag();
        ProductList productList= lists.get(i);
        Picasso.get().load(productList.getPicturelist())
                .into(holder.picturelist);
        holder.namelist.setText(productList.getName());
        holder.notelist.setText(productList.getNote());
        holder.starlist.setText(productList.getStar());
        holder.timelist.setText(productList.getTime());
        holder.rangelist.setText(productList.getRange());
        holder.salelist.setText(productList.getSale());
        holder.addressnearlist.setText(productList.getAddressnear());
        return view;
    }
}
