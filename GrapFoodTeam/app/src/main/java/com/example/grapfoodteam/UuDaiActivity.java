package com.example.grapfoodteam;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.grapfoodteam.adapter.DeliveryAdapter;
import com.example.grapfoodteam.model.Delivery;

import java.util.ArrayList;

public class UuDaiActivity extends AppCompatActivity {
ArrayList<Delivery> array;
DeliveryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getSupportActionBar().hide();
        setContentView(R.layout.activity_santhem_uudai);
        ListView listView = findViewById(R.id.lvUudai);
ImageView imageView = findViewById(R.id.bck);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        array=new ArrayList<>();
        array.add(new Delivery("10.000đ",R.drawable.delivery));
        array.add(new Delivery("30.000đ",R.drawable.delivery1));
        array.add(new Delivery("5.000đ",R.drawable.delivery2));
        array.add(new Delivery("40.000đ",R.drawable.delivery));
        array.add(new Delivery("50.000đ",R.drawable.delivery1));
        array.add(new Delivery("20.000đ",R.drawable.delivery2));

        adapter = new DeliveryAdapter(UuDaiActivity.this,R.layout.line_santhem,array);
        listView.setAdapter(adapter);

    }
}
