package com.example.grapfoodteam.model;

public class ProductGridDetail {
    private int id;
    private String picturegriddetail;
    private String namegriddetail;
    private String pricegriddetail;
    private String notegriddetail;

    public ProductGridDetail(int id, String picturegriddetail, String namegriddetail, String pricegriddetail, String notegriddetail) {
        this.id = id;
        this.picturegriddetail = picturegriddetail;
        this.namegriddetail = namegriddetail;
        this.pricegriddetail = pricegriddetail;
        this.notegriddetail = notegriddetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicturegriddetail() {
        return picturegriddetail;
    }

    public void setPicturegriddetail(String picturegriddetail) {
        this.picturegriddetail = picturegriddetail;
    }

    public String getNamegriddetail() {
        return namegriddetail;
    }

    public void setNamegriddetail(String namegriddetail) {
        this.namegriddetail = namegriddetail;
    }

    public String getPricegriddetail() {
        return pricegriddetail;
    }

    public void setPricegriddetail(String pricegriddetail) {
        this.pricegriddetail = pricegriddetail;
    }

    public String getNotegriddetail() {
        return notegriddetail;
    }

    public void setNotegriddetail(String notegriddetail) {
        this.notegriddetail = notegriddetail;
    }
}
