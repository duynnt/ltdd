package com.example.grapfoodteam.model;

public class profilefood {
    private int id;
    private String tenquan;
    private String theloai;
    private String danhgia;
    private String khuyenmai;
    private String thoigian;
    private String km;
    private String hinh;

    public profilefood(int id, String tenquan, String theloai, String danhgia, String khuyenmai, String thoigian, String km, String hinh) {
        this.id = id;
        this.tenquan = tenquan;
        this.theloai = theloai;
        this.danhgia = danhgia;
        this.khuyenmai = khuyenmai;
        this.thoigian = thoigian;
        this.km = km;
        this.hinh = hinh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenquan() {
        return tenquan;
    }

    public void setTenquan(String tenquan) {
        this.tenquan = tenquan;
    }

    public String getTheloai() {
        return theloai;
    }

    public void setTheloai(String theloai) {
        this.theloai = theloai;
    }

    public String getDanhgia() {
        return danhgia;
    }

    public void setDanhgia(String danhgia) {
        this.danhgia = danhgia;
    }

    public String getKhuyenmai() {
        return khuyenmai;
    }

    public void setKhuyenmai(String khuyenmai) {
        this.khuyenmai = khuyenmai;
    }

    public String getThoigian() {
        return thoigian;
    }

    public void setThoigian(String thoigian) {
        this.thoigian = thoigian;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }
}
