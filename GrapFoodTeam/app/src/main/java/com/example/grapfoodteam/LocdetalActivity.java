package com.example.grapfoodteam;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class LocdetalActivity extends AppCompatActivity {
    Animation topanim;
    LinearLayout linearLayout, lndexuat,lndanhgia, lnnoibat;
    Toolbar toolbar;
    ImageView imgtichdexuat, imgnb, imgdg;
    Button btntk;
    int loc = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locdetal);
        imgtichdexuat = (ImageView) findViewById(R.id.imagetich);
        imgnb = (ImageView) findViewById(R.id.imgnoibat);
        imgdg = (ImageView) findViewById(R.id.imgdanhgia);
        lndexuat = (LinearLayout) findViewById(R.id.lndexuat);
        lndanhgia = (LinearLayout) findViewById(R.id.lndanhgia);
        lnnoibat = (LinearLayout) findViewById(R.id.lnnoibat);
        btntk = (Button) findViewById(R.id.btntk);
        lndexuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgtichdexuat.setImageResource(R.drawable.ic_actionimg);
                imgnb.setImageResource(R.drawable.ic_click_icon);
                imgdg.setImageResource(R.drawable.ic_click_icon);
                loc = 2;
            }
        });
        lnnoibat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgtichdexuat.setImageResource(R.drawable.ic_click_icon);
                imgnb.setImageResource(R.drawable.ic_actionimg);
                imgdg.setImageResource(R.drawable.ic_click_icon);
                loc = 3;
            }
        });
        lndanhgia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgtichdexuat.setImageResource(R.drawable.ic_click_icon);
                imgnb.setImageResource(R.drawable.ic_click_icon);
                imgdg.setImageResource(R.drawable.ic_actionimg);
                loc = 4;
            }
        });
        btntk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loc == 2){
                    Intent intent= new Intent(LocdetalActivity.this,DetailGanToiActivity.class);
                    intent.putExtra("key",loc);
                    startActivity(intent);
                }else {
                    if (loc == 3){
                        Intent intent= new Intent(LocdetalActivity.this,DetailGanToiActivity.class);
                        intent.putExtra("key",loc);
                        startActivity(intent);
                    }else {
                        if (loc == 4){
                            Intent intent= new Intent(LocdetalActivity.this,DetailGanToiActivity.class);
                            intent.putExtra("key",loc);
                            startActivity(intent);
                        }else {
                            startActivity(new Intent(getApplicationContext(),DetailGanToiActivity.class));
                        }
                    }
                }
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbarloc);
        topanim = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        linearLayout = (LinearLayout) findViewById(R.id.lnanimationloc);
        linearLayout.setAnimation(topanim);
        toolbar.setNavigationIcon(R.drawable.ic_close_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),DetailGanToiActivity.class));
            }
        });
    }
}