package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MoneyActivity extends AppCompatActivity {
    ImageView imgBack,imgchoose,imgDoolar;
    Button btnThemPhuongthucThanhToan;
    TextView txtTienMat,txtHeadertitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money);
       anhxa();


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(com.example.grapfoodteam.MoneyActivity.this,CardActivity.class);
                startActivity(intent);
            }
        });
        btnThemPhuongthucThanhToan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnThemPhuongthucThanhToan.setVisibility(view.GONE);
                imgchoose.setImageResource(R.drawable.card);
                imgchoose.setVisibility(view.GONE);
                txtTienMat.setText("Card");
                txtHeadertitle.setText("Thêm phương thức thanh toán");
            }
        });
    }
    private  void anhxa()

    {
        txtHeadertitle=(TextView) findViewById(R.id.header_title);
        imgchoose=(ImageView) findViewById(R.id.imageChoose);
        imgDoolar=(ImageView) findViewById(R.id.dollar);
        txtTienMat=(TextView) findViewById(R.id.textViewTien);
        imgBack=(ImageView) findViewById(R.id.back);
        btnThemPhuongthucThanhToan=(Button) findViewById(R.id.buttonThemPhuongThucThanhToan);
    }
}