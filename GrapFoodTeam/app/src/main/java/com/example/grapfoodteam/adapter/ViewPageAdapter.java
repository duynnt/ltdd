package com.example.grapfoodteam.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.grapfoodteam.fragment.DoiDiemFragment;
import com.example.grapfoodteam.fragment.UuDaiFragment;

public class ViewPageAdapter extends FragmentStatePagerAdapter {
    public ViewPageAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new DoiDiemFragment();
            case 1:
                return new UuDaiFragment();
            default:
                return new DoiDiemFragment();

        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position)
        {
            case 0:
                title="Đổi Điểm";
                break;
            case 1:
                title="Ưu Đãi";
                break;
        }

        return title;
    }
}

