package com.example.grapfoodteam.model;

public class Product {
    private  int id;
    private String promotion;
    private String picture;
    private String range;
    private String name;
    private String sale;

    public Product(int id, String promotion, String picture, String range, String name, String sale) {
        this.id = id;
        this.promotion = promotion;
        this.picture = picture;
        this.range = range;
        this.name = name;
        this.sale = sale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }
}
