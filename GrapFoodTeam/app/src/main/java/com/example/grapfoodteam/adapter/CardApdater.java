package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.grapfoodteam.model.Card;
import com.example.grapfoodteam.R;
import com.example.grapfoodteam.UpdateCartActivity;

import java.text.DecimalFormat;
import java.util.List;

public class CardApdater extends BaseAdapter {
    private Context context;
    private List<Card> cardList;
    private int layout;

    public CardApdater(Context context, List<Card> cardList, int layout) {
        this.context = context;
        this.cardList = cardList;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return cardList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder {
        TextView txt_card_name, txt_card_qty, txt_card_chinhsua, txt_card_note,txt_card_tongtien;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.txt_card_name = (TextView) view.findViewById(R.id.text_card_name);
            holder.txt_card_note = (TextView) view.findViewById(R.id.text_card_Luuydacbiet);
            holder.txt_card_chinhsua = (TextView) view.findViewById(R.id.text_card_chinhsua);
            holder.txt_card_qty = (TextView) view.findViewById(R.id.text_card_qty);
            holder.txt_card_tongtien=(TextView) view.findViewById(R.id.text_card_tongtien);
            view.setTag(holder);


        } else {
            holder = (ViewHolder) view.getTag();
        }
        final Card card = cardList.get(i);
        holder.txt_card_name.setText(card.getName());
        holder.txt_card_qty.setText(card.getQty()+"x");
        holder.txt_card_note.setText(card.getNote());
        DecimalFormat format=new DecimalFormat("###,###.###");
        int giatien=card.getPrice();
        int soluong=card.getQty();
        int tongtien=0;
        tongtien+=giatien*soluong;
        holder.txt_card_tongtien.setText(format.format(tongtien));

        String note=card.getNote();
        if(!note.isEmpty())
        {
            holder.txt_card_note.setVisibility(View.VISIBLE);
            holder.txt_card_note.setText(note);
        }
        holder.txt_card_chinhsua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, UpdateCartActivity.class);
                intent.putExtra("hinh",card.getImage());
                intent.putExtra("ten",card.getName());
                intent.putExtra("gia",card.getPrice());
                intent.putExtra("luuy",card.getNote());
                intent.putExtra("soluong",card.getQty());
                intent.putExtra("cartid",card.getCard_id());
                context.startActivity(intent);
            }
        });
        return view;

    }
}

