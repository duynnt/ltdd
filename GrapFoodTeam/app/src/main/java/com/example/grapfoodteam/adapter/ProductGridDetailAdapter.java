package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grapfoodteam.R;
import com.example.grapfoodteam.model.ProductGridDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductGridDetailAdapter extends BaseAdapter {
    private Context context;
    private List<ProductGridDetail> list;
    private int layout;

    public ProductGridDetailAdapter(Context context, List<ProductGridDetail> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolderDetail{
        ImageView pictureDetail;
        TextView txtNameDetail,txtPriceDetail,txtNoteDetail;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderDetail holderDetail;
        if(view==null)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(layout,null);
            holderDetail=new ViewHolderDetail();
            holderDetail.pictureDetail=(ImageView) view.findViewById(R.id.image_productdetailgrid);
            holderDetail.txtNameDetail=(TextView) view.findViewById(R.id.txtnameproductdetailgrid);
            holderDetail.txtPriceDetail=(TextView) view.findViewById(R.id.txtpriceproductdetailgrid);
            holderDetail.txtNoteDetail=(TextView) view.findViewById(R.id.txtnoteproductdetailgrid);
            view.setTag(holderDetail);
        }
        else
            holderDetail= (ViewHolderDetail) view.getTag();
        ProductGridDetail productGridDetail=list.get(i);
        Picasso.get().load(productGridDetail.getPicturegriddetail())
                .into(holderDetail.pictureDetail);
        holderDetail.txtNameDetail.setText(productGridDetail.getNamegriddetail());
        holderDetail.txtPriceDetail.setText(productGridDetail.getPricegriddetail());
        holderDetail.txtNoteDetail.setText(productGridDetail.getNotegriddetail());
        return view;
    }
}
