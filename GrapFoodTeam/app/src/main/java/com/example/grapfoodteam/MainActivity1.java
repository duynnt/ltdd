package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity1 extends AppCompatActivity {
Animation topAni,botAni,RAni,LAni;
ImageView im1,im2;
TextView t1,t2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main1);
        new Timer().schedule(new TimerTask(){
                public void run() {
                    startActivity(new Intent(MainActivity1.this, TrangChuActivity.class));
                    finish();
                }
        }, 2500);
       // getSupportActionBar().hide();
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    topAni = AnimationUtils.loadAnimation(this,R.anim.top_animation1);
        RAni = AnimationUtils.loadAnimation(this,R.anim.right);
        LAni = AnimationUtils.loadAnimation(this,R.anim.left);
    im1 = findViewById(R.id.im1);
    im2 = findViewById(R.id.im2);
        t1 = findViewById(R.id.tx);
        t2 = findViewById(R.id.tx1);
    im1.setAnimation(topAni);
    im2.setAnimation(topAni);
    t1.setAnimation(RAni);
    t2.setAnimation(LAni);


    }
}