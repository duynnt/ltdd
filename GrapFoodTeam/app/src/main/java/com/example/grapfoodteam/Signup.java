package com.example.grapfoodteam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.util.Connect;

import java.util.HashMap;
import java.util.Map;

public class Signup extends AppCompatActivity {
    Button btndn, btndk;
    EditText edtsdt, editpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        btndn = (Button) findViewById(R.id.dangnhapbtn);
        btndk = (Button) findViewById(R.id.continues);
        edtsdt = (EditText) findViewById(R.id.editsdt);
        editpass  = (EditText) findViewById(R.id.passwork);
        btndn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Signup.this,login.class));
            }
        });
        btndk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtsdt.getText().toString().trim() != "" && editpass.getText().toString().trim() != ""){
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Connect.ConnectUrl("ltdd/webservice/AndroidTeam/insertuser.php"), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.equals("TC")){
                                Toast.makeText(Signup.this, "Đăng kí thành công", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(Signup.this, "Tài khoản đã tồn tại", Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(Signup.this, "sever", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> param = new HashMap<>();
                            param.put("taikhoan",edtsdt.getText().toString().trim());
                            param.put("matkhau",editpass.getText().toString().trim());
                            return param;
                        }
                    };
                    requestQueue.add(stringRequest);
                }else {
                    Toast.makeText(Signup.this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}