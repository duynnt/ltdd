package com.example.grapfoodteam.model;

public class Card {
    public int card_id;
    public int product_id;
    public String name;
    public String image;
    public int qty;
    public int price;
    public String note;

    public Card(int card_id, int product_id, String name, String image, int qty, int price, String note) {
        this.card_id = card_id;
        this.product_id = product_id;
        this.name = name;
        this.image = image;
        this.qty = qty;
        this.price = price;
        this.note = note;
    }

    public int getCard_id() {
        return card_id;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
