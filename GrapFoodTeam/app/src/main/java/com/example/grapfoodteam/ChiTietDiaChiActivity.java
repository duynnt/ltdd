package com.example.grapfoodteam;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ChiTietDiaChiActivity extends AppCompatActivity {
    ImageView imgGoogleMap,imgClose,imgXoa;
    EditText edtLocation;
    Intent myIntent;
    TextView txtThemNha,txtLocation;
    LinearLayout lnThemNha;
    int PLACE_PIKER_REQUEST=1;
    FusedLocationProviderClient fusedLocationProviderClient;
    private final static int LOCATION_REQUEST_CODE = 23;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_dia_chi);
        Anhxa();
        lnThemNha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChiTietDiaChiActivity.this,ThemNhaActivity.class));
            }
        });

        //Láy dữ liệu 2
        Intent intent2=getIntent();
        Bundle bundle2=intent2.getBundleExtra("dataDiaChi");
        if(bundle2!=null)
        {

            String location=bundle2.getString("address");
            edtLocation.setText(location);
        }
        //Kết thúc lấy dữ liệu


        //Láy dữ liệu
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("dulieuDiaChi");
        if(bundle!=null)
        {

            String diachi=bundle.getString("diachi");
            if(diachi.equals(""))
            {
                txtThemNha.setText("Thêm nhà");

            }
            else
            {
                txtThemNha.setText(diachi);

            }

        }
        //Kết thúc lấy dữ liệu
        //get curent location
        fusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
        if(ActivityCompat.checkSelfPermission(ChiTietDiaChiActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
        {
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location=task.getResult();
                    if(location!=null)
                    {
                        Geocoder geocoder=new Geocoder(ChiTietDiaChiActivity.this, Locale.getDefault());
                        try {
                            List<Address> addressList=geocoder.getFromLocation(location.getLatitude(),
                                    location.getLongitude(),1);

                            txtLocation.setText(addressList.get(0).getAddressLine(0));
                        }catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Toast.makeText(ChiTietDiaChiActivity.this,"Location null error",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }else
        {
            ActivityCompat.requestPermissions(ChiTietDiaChiActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);
        }
        //
        imgXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtLocation.setText("");
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                truyenDuLieu();
                startActivity(myIntent);
            }
        });
        imgGoogleMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChiTietDiaChiActivity.this,MapActivity.class));
            }
        });
    }
    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PIKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stringBuilder=new StringBuilder();
                String latitude=String.valueOf(place.getLatLng().latitude);
                String longitude=String.valueOf(place.getLatLng().longitude);
                String address=String.format("Place: %s",place.getAddress());

                stringBuilder.append("LATITUDE:");
                stringBuilder.append(latitude);
                stringBuilder.append("\n");
                stringBuilder.append("LONGITUDE");
                stringBuilder.append(longitude);
                stringBuilder.append("\n");
                stringBuilder.append("Address");
                stringBuilder.append(address);
                edtLocation.setText(stringBuilder.toString());
                //String address = String.format("Place: %s", place.getAddress());
                //txtPlacePiker.setText(address);
            }
        }
    }*/
    public void truyenDuLieu()
    {
        myIntent=new Intent(ChiTietDiaChiActivity.this, CardActivity.class);
        String location=edtLocation.getText().toString();
        Bundle bundle=new Bundle();
        bundle.putString("diachi",location);
        myIntent.putExtra("dulieuDiaChi", bundle);
        //myIntent.putExtra("dulieu",arrayCourse);
        // myIntent.putExtra("dulieu",2017);

    }
    private void Anhxa()
    {
        txtLocation=(TextView) findViewById(R.id.textviewLocation);
        txtThemNha=(TextView) findViewById(R.id.textviewThemNha);
        imgGoogleMap=(ImageView) findViewById(R.id.imageGoogleMap);
        imgClose=(ImageView) findViewById(R.id.imageClose);
        imgXoa=(ImageView) findViewById(R.id.imageXoa);
        edtLocation=(EditText) findViewById(R.id.editTextLocation);
        lnThemNha=(LinearLayout) findViewById(R.id.linearThemNha);
    }
}