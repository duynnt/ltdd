package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Search extends AppCompatActivity {
    ListView listView;
    String [] lstSource ={
            "Món Hàn - Món Thái - Món Nhật",
            "Ăn vặt",
            "Bánh mì - Xôi - Bánh mặn",
            "Bún - Phở - Cháo",
            "Cà phê - Trà - Sinh tố - Nước ép",
            "Chè - Sữa chua - Tráng miệng",
            "Cơm",
            "Cơm chay - Salad Healthy - Đồ chay",
            "Gà rán - Burger",
            "Khác"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView toYeuthich = (TextView)findViewById(R.id.ts);
        setContentView(R.layout.activity_search);
        ImageView imageView = (ImageView) findViewById(R.id.back_yeuthich);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Search.this, TrangChuActivity.class);
                startActivity(intent);
            }
        });
        listView = (ListView) findViewById(R.id.lstView);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lstSource);
        listView.setAdapter(adapter);
//
    }
}