package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.example.grapfoodteam.model.Food;
import com.example.grapfoodteam.R;



import java.util.List;
public class FoodAdapter extends BaseAdapter{
    private Context context;
    private int layout;
    private List<Food> foodList;
    Food food;

    public FoodAdapter(Context context, int layout, List<Food> foodList) {
        this.context = context;
        this.layout = layout;
        this.foodList = foodList;
    }

    @Override
    public int getCount() {
        return foodList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public class ViewHolder{
        TextView txtTen,txtGia;
        ImageView imgHinh;
        ImageView im;
        TextView textView;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if(view==null){
            holder= new ViewHolder();
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout,null);
            holder.txtTen  = (TextView)view.findViewById(R.id.ten);
            holder.txtGia  = (TextView)view.findViewById(R.id.gia);
            holder.imgHinh = (ImageView)view.findViewById(R.id.hinh);
            holder.textView=(TextView) view.findViewById(R.id.luuy);
            view.setTag(holder);
        }
        else{
            holder= (ViewHolder) view.getTag();
        }
        Animation animation = AnimationUtils.loadAnimation(context,R.anim.scale);
        holder.textView.setAnimation(animation);
        food = foodList.get(position);
        holder.txtTen.setText(food.getTen());
        holder.txtGia.setText(food.getGia()+ " VNĐ");
        Picasso.get().load(food.getHinh())
                .placeholder(R.drawable.d1)
                .into(holder.imgHinh);
        Animation animation1 = AnimationUtils.loadAnimation(context,R.anim.slide_left);
        view.startAnimation(animation1);
        return view;
    }
}
