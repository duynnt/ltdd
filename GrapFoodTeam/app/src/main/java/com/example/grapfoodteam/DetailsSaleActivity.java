package com.example.grapfoodteam;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class DetailsSaleActivity extends AppCompatActivity {
    TextView txt_dl_sale_name,txt_dl_sale_start,txt_dl_sale_finish,txt_dl_sale_des;
    ImageView img_dl_bk_sale;
    ImageButton img_dl_sale;
    ImageView ic_close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_sale);
        Anhxa();
        Bundle bundle=getIntent().getExtras();
        txt_dl_sale_name.setText(bundle.getString("ten_sale"));
        txt_dl_sale_des.setText(bundle.getString("description_sale"));
        txt_dl_sale_start.setText(bundle.getString("start_sale"));
        txt_dl_sale_finish.setText(bundle.getString("finish_sale"));
        String link_image=bundle.getString("image_sale");
        String link_bk=bundle.getString("background_sale");
        Picasso.get().load(link_image)
                .placeholder(R.drawable.pizza1)
                .error(R.drawable.pizza2)
                .into(img_dl_sale);
        Picasso.get().load(link_bk)
                .placeholder(R.drawable.pizza1)
                .error(R.drawable.pizza2)
                .into(img_dl_bk_sale);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void Anhxa()
    {
        txt_dl_sale_name=(TextView) findViewById(R.id.text_detail_sale_name);
        txt_dl_sale_des=(TextView) findViewById(R.id.text_detail_sale_des);
        txt_dl_sale_start=(TextView) findViewById(R.id.text_dl_sale_start);
        txt_dl_sale_finish=(TextView) findViewById(R.id.text_dl_sale_finish);
        img_dl_sale=(ImageButton) findViewById(R.id.image_dl_sale);
        img_dl_bk_sale=(ImageView) findViewById(R.id.image_dl_bk_sale);
        ic_close=(ImageView) findViewById(R.id.image_ic_close);
    }
}