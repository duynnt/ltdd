package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grapfoodteam.R;
import com.example.grapfoodteam.model.Sale;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SaleApdater extends BaseAdapter {
    private Context context;
    private List<Sale> saleList;
    private int layout;

    public SaleApdater(Context context, List<Sale> saleList, int layout) {
        this.context = context;
        this.saleList = saleList;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return saleList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder {
        TextView txt_sale_name, txt_sale_hieulucden;
        ImageView img_sale;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.txt_sale_name = (TextView) view.findViewById(R.id.text_sale_ten);
            holder.txt_sale_hieulucden = (TextView) view.findViewById(R.id.text_sale_hieulucden);
            holder.img_sale=(ImageView) view.findViewById(R.id.image_sale);
            view.setTag(holder);


        } else {
            holder = (ViewHolder) view.getTag();
        }
        final Sale sale = saleList.get(i);
        holder.txt_sale_name.setText(sale.getNameSale());
        holder.txt_sale_hieulucden.setText(sale.getDayFinish());
        Picasso.get().load(sale.getImageSale()).into(holder.img_sale);
        return view;

    }
}
