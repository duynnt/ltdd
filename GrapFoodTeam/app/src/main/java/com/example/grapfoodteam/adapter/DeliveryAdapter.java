package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grapfoodteam.model.Delivery;
import com.example.grapfoodteam.R;

import java.util.List;

public class DeliveryAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Delivery> deliveryList;
    Delivery delivery;

    public DeliveryAdapter(Context context, int layout, List<Delivery> deliveryList) {
        this.context = context;
        this.layout = layout;
        this.deliveryList = deliveryList;
    }

    @Override
    public int getCount() {
        return deliveryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ViewHolder {
        TextView tien;
        ImageView imgHinh;
Button bt;
        TextView text;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.tien = (TextView) view.findViewById(R.id.txtTien);
            holder.imgHinh = (ImageView) view.findViewById(R.id.img);
            holder.text = (TextView) view.findViewById(R.id.text);
            holder.bt= (Button)view.findViewById(R.id.button);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        delivery = deliveryList.get(position);
        holder.tien.setText(delivery.getTien());
        holder.imgHinh.setImageResource(delivery.getHinh());
        holder.text.setText("Giảm "+delivery.getTien()+" khi đặt...");
        holder.bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"Đổi điểm",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}