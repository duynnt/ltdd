package com.example.grapfoodteam.model;

public class ProductList {
    private int id;
    private String picturelist;
    private String name;
    private String note;
    private String star;
    private String time;
    private String range;
    private String sale;
    private String addressnear;

    public ProductList(int id, String picturelist, String name, String note, String star, String time, String range, String sale, String addressnear) {
        this.id = id;
        this.picturelist = picturelist;
        this.name = name;
        this.note = note;
        this.star = star;
        this.time = time;
        this.range = range;
        this.sale = sale;
        this.addressnear = addressnear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicturelist() {
        return picturelist;
    }

    public void setPicturelist(String picturelist) {
        this.picturelist = picturelist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getAddressnear() {
        return addressnear;
    }

    public void setAddressnear(String addressnear) {
        this.addressnear = addressnear;
    }
}
