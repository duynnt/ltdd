package com.example.grapfoodteam;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.model.ProductGridDetail;
import com.example.grapfoodteam.util.Connect;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ThemGioHang extends AppCompatActivity {
    RelativeLayout layoutnote;
    TextView txtNote,txtThemGioHang,txtCount,txtName,txtPrice;
    Button btnTang,btnGiam;
    ImageView banner;
    int amount=1;
    int idProduct=0;
    int idShop=0;
    int check=0;
    String imagebaner="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_gio_hang);
        Intent intent_result=getIntent();
        Bundle bundle_result=intent_result.getBundleExtra("idProduct");
        if(bundle_result!=null)
            idProduct=bundle_result.getInt("id");
        mapping();
        layoutnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                note();
            }
        });
        btnTang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                amount++;
                txtCount.setText(""+amount);
                txtThemGioHang.setText("Thêm vào giỏ hàng - "+amount*Integer.parseInt(txtPrice.getText().toString())+"đ");
            }
        });
        btnGiam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(amount==1)
                {
                    amount--;
                    txtCount.setText("" + amount);
                    txtThemGioHang.setText("Quay lại thực đơn");
                }
                else if(amount>0){
                    amount--;
                    txtCount.setText("" + amount);
                    txtThemGioHang.setText("Thêm vào giỏ hàng - "+amount*Integer.parseInt(txtPrice.getText().toString())+"đ");
                }
            }
        });
        txtThemGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(amount==0)
                    finish();
                else
                {
                    getCart(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/cart.php"),idShop+"");
                    Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int id=idShop;
                            Intent intent=new Intent(ThemGioHang.this,Detail.class);
                            Bundle bundle=new Bundle();
                            bundle.putInt("id",id);
                            intent.putExtra("idShop",bundle);
                            startActivity(intent);
                        }
                    },1500);
                }
            }
        });
        GetDuLieu(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/productid.php"),String.valueOf(idProduct));
    }
    private void mapping(){
        txtNote=(TextView) findViewById(R.id.txtNoteDialogDetail);
        layoutnote=(RelativeLayout) findViewById(R.id.layoutNote);
        txtThemGioHang=(TextView) findViewById(R.id.txtThemGioHangDetail);
        txtCount=(TextView) findViewById(R.id.txtcount);
        btnTang=(Button) findViewById(R.id.btnincreaseDetail);
        btnGiam=(Button) findViewById(R.id.btnreductionDetail);
        txtName=(TextView) findViewById(R.id.txtNameThemGioHangDetail);
        txtPrice=(TextView) findViewById(R.id.txtPriceThemGioHangDetail);
        banner=(ImageView) findViewById(R.id.BannerThemGioHangDetail);
    }
    private  void note(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_ghichudetail);
        dialog.setCanceledOnTouchOutside(true);
        ImageButton btnClose=(ImageButton) dialog.findViewById(R.id.btnCloseDialogNoteDetail);
        Button btnXacNhan=(Button) dialog.findViewById(R.id.btnXacnhanDialogNoteDetail);
        final EditText inputnote=(EditText) dialog.findViewById(R.id.txtinputNoteDialogDetail);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        btnXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtNote.setText(inputnote.getText().toString());
                dialog.cancel();
            }
        });
        dialog.show();
    }
    private void GetDuLieu(String url, final String id){
        RequestQueue requestQueue= Volley.newRequestQueue(ThemGioHang.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                imagebaner=jsonObject.getString("image");
                                Picasso.get().load(jsonObject.getString("image"))
                                        .into(banner);
                                txtName.setText(jsonObject.getString("name"));
                                txtPrice.setText(jsonObject.getString("price"));
                                txtThemGioHang.setText("Thêm vào giỏ hàng - "+jsonObject.getInt("price")+"đ");
                                idShop=jsonObject.getInt("idShop");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ThemGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("id",id);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void insertCart(String url,final String name,final String icon,final String sl,final String price,final String note,final String idSP, final String idShop){
        RequestQueue requestQueue=Volley.newRequestQueue(ThemGioHang.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ThemGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("name",name);
                parems.put("icon",icon);
                parems.put("sl",sl);
                parems.put("price",price);
                parems.put("note",note);
                parems.put("idSP",idSP);
                parems.put("idShop",idShop);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void UpdateCart(String url, final String id,final String qty){
        RequestQueue requestQueue=Volley.newRequestQueue(ThemGioHang.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ThemGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("id",id);
                parems.put("qty",qty);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void getCart(String url, final String id){
        RequestQueue requestQueue= Volley.newRequestQueue(ThemGioHang.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                if(jsonArray.length()==0)
                                {
                                    insertCart(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/insertcart.php"),
                                            txtName.getText().toString(),
                                            imagebaner,
                                            amount+"",
                                            txtPrice.getText().toString(),
                                            txtNote.getText().toString(),
                                            idProduct+"",
                                            idShop+""
                                            );
                                }
                                else
                                {
                                    for(int i=0;i<jsonArray.length();i++){
                                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                                        if(idProduct==jsonObject.getInt("idproduct")){
                                            UpdateCart(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/updatecart.php"),
                                                    jsonObject.getInt("id")+"",
                                                    jsonObject.getInt("qty")+amount+"");
                                            check=1;
                                            break;
                                        }
                                    }
                                    if(check==0)
                                    {
                                        insertCart(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/insertcart.php"),
                                                txtName.getText().toString(),
                                                imagebaner,
                                                amount+"",
                                                txtPrice.getText().toString(),
                                                txtNote.getText().toString(),
                                                idProduct+"",
                                                idShop+""
                                        );
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ThemGioHang.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("id",id);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
}