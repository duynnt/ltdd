package com.example.grapfoodteam.model;

public class Sale {
    int id;
    String nameSale;
    String imageSale;
    String backgroundSale;
    String dayStart;
    String dayFinish;
    String descriptionSale;

    public Sale() {
    }

    public Sale(int id, String nameSale, String imageSale, String backgroundSale, String dayStart, String dayFinish, String descriptionSale) {
        this.id = id;
        this.nameSale = nameSale;
        this.imageSale = imageSale;
        this.backgroundSale = backgroundSale;
        this.dayStart = dayStart;
        this.dayFinish = dayFinish;
        this.descriptionSale = descriptionSale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameSale() {
        return nameSale;
    }

    public void setNameSale(String nameSale) {
        this.nameSale = nameSale;
    }

    public String getImageSale() {
        return imageSale;
    }

    public void setImageSale(String imageSale) {
        this.imageSale = imageSale;
    }

    public String getBackgroundSale() {
        return backgroundSale;
    }

    public void setBackgroundSale(String backgroundSale) {
        this.backgroundSale = backgroundSale;
    }

    public String getDayStart() {
        return dayStart;
    }

    public void setDayStart(String dayStart) {
        this.dayStart = dayStart;
    }

    public String getDayFinish() {
        return dayFinish;
    }

    public void setDayFinish(String dayFinish) {
        this.dayFinish = dayFinish;
    }

    public String getDescriptionSale() {
        return descriptionSale;
    }

    public void setDescriptionSale(String descriptionSale) {
        this.descriptionSale = descriptionSale;
    }
}
