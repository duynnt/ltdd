package com.example.grapfoodteam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.model.information;
import com.example.grapfoodteam.util.Connect;
import com.example.grapfoodteam.login;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class profile extends AppCompatActivity {
    androidx.appcompat.widget.Toolbar toolbarlogin;
    EditText editTextten, editTextsdt, editTextemail;
    ArrayList<information> informationList;
    Button btnupdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        informationList = new ArrayList<>();
        editTextten = (EditText) findViewById(R.id.edittenht);
        editTextsdt = (EditText) findViewById(R.id.editsdtht);
        editTextemail = (EditText) findViewById(R.id.editemailht);
        btnupdate = (Button) findViewById(R.id.btnupdates);
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushdata();
            }
        });
        toolbarlogin = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        toolbarlogin.setNavigationIcon(R.drawable.ic_back);
        toolbarlogin.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),TrangChuActivity.class));
            }
        });

        getdata();
    }
    private void pushdata(){
        RequestQueue requestQueue = Volley.newRequestQueue(profile.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/updateprofile.php"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("success")){
                            Toast.makeText(profile.this, "Cập nhập thành công", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(profile.this, "Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(profile.this, "Lỗi kết nối", Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param.put("id","1");
                param.put("name",editTextten.getText().toString().trim());
                param.put("sdt",editTextsdt.getText().toString().trim());
                param.put("email",editTextemail.getText().toString().trim());
                return param;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void getdata(){
        RequestQueue requestQueue = Volley.newRequestQueue(profile.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/getdataprofile.php"), null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        informationList.clear();
                            try {
                                JSONObject jsonObject = response.getJSONObject(0);
                                informationList.add(new information(
                                        jsonObject.getInt("Id"),
                                        jsonObject.getString("Ten"),
                                        jsonObject.getString("SoDienThoai"),
                                        jsonObject.getString("Email")
                                ));
                                editTextten.setText(informationList.get(0).getTen());
                                editTextsdt.setText(informationList.get(0).getSdt());
                                editTextemail.setText(informationList.get(0).getEmail());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(profile.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }

        );
        requestQueue.add(jsonArrayRequest);
    }
}