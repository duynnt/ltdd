package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.model.Users;
import com.example.grapfoodteam.util.Connect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class login extends AppCompatActivity {
    Button btndn;
    EditText editsdt,editpass;
    TextView txtdk;
    ArrayList<Users> users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btndn = (Button) findViewById(R.id.continues);
        editsdt = (EditText) findViewById(R.id.editsdt);
        editpass = (EditText) findViewById(R.id.passwork);
        txtdk = (TextView) findViewById(R.id.txtdangki);
        users = new ArrayList<>();
        txtdk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(login.this,Signup.class));
            }
        });
        btndn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tg =0;
                for (int i=0 ; i< users.size(); i++){
                    if (editsdt.getText().toString().trim().equals(users.get(i).getName()) && editpass.getText().toString().trim().equals(users.get(i).getPass())){
                        tg =1;
                        break;
                    }
                }
                if (tg == 1){
                    startActivity(new Intent(login.this,MainActivity1.class));Toast.makeText(login.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(login.this, "Tài khoản hoặc mật khẩu sai", Toast.LENGTH_SHORT).show();
                }
            }
        });
        getdata();
    }

    private void getdata(){
        RequestQueue requestQueue = Volley.newRequestQueue(login.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Connect.ConnectUrl("ltdd/webservice/AndroidTeam/selectUser.php"), null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i=0; i < response.length(); i++){
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                users.add(new Users(
                                   jsonObject.getInt("Id"),
                                   jsonObject.getString("HoTen"),
                                   jsonObject.getString("Pass")
                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
}
//    RequestQueue requestQueue = Volley.newRequestQueue(login.this);
//    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, "http://192.168.42.28/AndroidTeam/selectUser.php", null,
//            new Response.Listener<JSONArray>() {
//                @Override
//                public void onResponse(JSONArray response) {
//                    for (int i=0 ; i < response.length() ; i++){
//                        try {
//                            JSONObject jsonObject = response.getJSONObject(i);
//                            users.add(new Users(
//                                    jsonObject.getInt("Id"),
//                                    jsonObject.getString("HoTen"),
//                                    jsonObject.getString("Pass")
//                            ));
//                            Toast.makeText(login.this, "Thanh Cong", Toast.LENGTH_SHORT).show();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//
//                        }
//                    }
//                }
//            },
//            new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//
//                }
//            }
//    );
//        requestQueue.add(jsonArrayRequest);