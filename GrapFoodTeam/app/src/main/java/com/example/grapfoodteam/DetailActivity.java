package com.example.grapfoodteam;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.model.CodeUuDai;
import com.example.grapfoodteam.util.Connect;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends AppCompatActivity {
    Dialog dialog;
    EditText editText;
    ArrayList<CodeUuDai> arrayList;
    int idCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail1);

        Intent myItent =  getIntent();
        TextView textView = findViewById(R.id.txt);
        textView.setText("16 thg 11 2020 to 22 thg 11 2020");
        textView.setSelected(true);
        ImageView img= findViewById(R.id.logo);
        Button bt = findViewById(R.id.back);
        Animation topAni = AnimationUtils.loadAnimation(this,R.anim.animation);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.activitytsx);
        Picasso.get().load(myItent.getStringExtra("hinh")).into(img);
        Button bt1 = findViewById(R.id.bt_use);
        arrayList = new ArrayList<>();
        getdata();
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Toast.makeText(DetailActivity.this,"Bạn chưa đủ điểm",Toast.LENGTH_SHORT).show();*/
                dialog.show();
            }
        });
        //ImageView imageView = dialog.findViewById(R.id.sale);
        //imageView.setAnimation(topAni);
        editText = dialog.findViewById(R.id.edtTen);
        Button button = dialog.findViewById(R.id.btnThem);
        Button button1 = dialog.findViewById(R.id.exit);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int dem =0;
                for (int i=0 ; i< arrayList.size(); i++){
                    if (editText.getText().toString().trim().equals(arrayList.get(i).getMaCode())){
                        dem =1;
                        idCode= arrayList.get(i).getId();
                        break;
                    }
                }
                if (dem == 1){
                    Toast.makeText(DetailActivity.this, "Mã xác thực sẽ gửi qua SMS của bạn trong 15s tới! ", Toast.LENGTH_SHORT).show();
                    xoaCode();
                }else{
                    Toast.makeText(DetailActivity.this, "Mã code không hợp lệ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void getdata(){
        RequestQueue requestQueue = Volley.newRequestQueue(DetailActivity.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Connect.ConnectUrl("/ltdd/webservice/AndroidTeam/linh/selectCode.php"), null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i=0; i < response.length(); i++){
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                arrayList.add(new CodeUuDai(
                                        jsonObject.getInt("Id"),
                                        jsonObject.getString("Code")

                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private void xoaCode(){
        RequestQueue requestQueue = Volley.newRequestQueue(DetailActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Connect.ConnectUrl("/ltdd/webservice/AndroidTeam/linh/deleteCode.php"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("IDCODE",Integer.toString(idCode));

                return prams;
            }
        }
                ;
        requestQueue.add(stringRequest);
    }
}
