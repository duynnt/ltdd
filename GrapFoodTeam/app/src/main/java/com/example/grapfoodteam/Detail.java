package com.example.grapfoodteam;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.adapter.ProductGridDetailAdapter;
import com.example.grapfoodteam.model.ProductGridDetail;
import com.example.grapfoodteam.util.Connect;
import com.example.grapfoodteam.util.ExpandableHeightGridView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Detail extends AppCompatActivity {
    private Menu menu;
    private int favorite=0;
    ImageView baner;
    ExpandableHeightGridView gridDetail;
    ArrayList<ProductGridDetail> arrayList1,arrayList2;
    ProductGridDetailAdapter adapter1,adapter2;
    //
    ExpandableHeightListView listView1;
    TextView txtNameDetail,txtNoteDetail,txtTotalPriceGH,txtTotalQtyGH;
    RelativeLayout layoutGioHang;
    int idShop=0;
    int priceTotal=0;
    int qtyTotal=0;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        txtNameDetail=(TextView) findViewById(R.id.txtNameDetail);
        txtNoteDetail=(TextView) findViewById(R.id.txtnotedetail) ;
        txtTotalPriceGH=(TextView) findViewById(R.id.txtTotalPriceGH_Detail);
        txtTotalQtyGH=(TextView) findViewById(R.id.txtTotalQtyGH_Detail);
        baner=(ImageView) findViewById(R.id.baner_detail);
        layoutGioHang=(RelativeLayout) findViewById(R.id.layoutGioHang);
        layoutGioHang.setVisibility(View.GONE);
        final Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backwhite);
        final CollapsingToolbarLayout collapsingToolbarLayout=findViewById(R.id.collapsingToolbarlayout);
        AppBarLayout appBarLayout=findViewById(R.id.appBarlayout);
        toolbar.setTitle("");
        collapsingToolbarLayout.setTitleEnabled(false);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isVisible = true;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    toolbar.setTitle(txtNameDetail.getText().toString());
                    isVisible = true;
                } else if(isVisible) {
                    toolbar.setTitle("");
                    isVisible = false;
                }
            }
        });
        Intent intent_result=getIntent();
        Bundle bundle_result=intent_result.getBundleExtra("idShop");
        if(bundle_result!=null)
            idShop=bundle_result.getInt("id");
       layoutGioHang.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               startActivity(new Intent(Detail.this,CardActivity.class));
           }
       });
       GetDuLieuShop(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shopid.php"),String.valueOf(idShop));
        mappingGrid();
        mappingList1();
        CheckCart(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/cart.php"),String.valueOf(idShop));
        gridDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int id=arrayList1.get(i).getId();
                Intent intent=new Intent(Detail.this,ThemGioHang.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                intent.putExtra("idProduct",bundle);
                startActivity(intent);
            }
        });
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int id=arrayList2.get(i).getId();
                Intent intent=new Intent(Detail.this,ThemGioHang.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",id);
                intent.putExtra("idProduct",bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu=menu;
        getMenuInflater().inflate(R.menu.actionbardetail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                startActivity(new Intent(Detail.this,TrangChuActivity.class));
                break;
            case R.id.menu_favorite:
                if(favorite==0)
                {
                    favorite=1;
                    menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.tim));
                    UpdateShopTym(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/updateshoptym.php"),String.valueOf(idShop),String.valueOf(favorite));
                }
                else {
                    favorite = 0;
                    menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.heartwhite));
                    UpdateShopTym(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/updateshoptym.php"),String.valueOf(idShop),String.valueOf(favorite));
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private  void mappingList1(){
        listView1=(ExpandableHeightListView) findViewById(R.id.listviewDetail1);
        arrayList2=new ArrayList<>();
        adapter2=new ProductGridDetailAdapter(com.example.grapfoodteam.Detail.this,arrayList2,R.layout.line_productlist_detail);
        listView1.setAdapter(adapter2);
        listView1.setExpanded(true);
        GetDuLieuAll(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/productall.php"),String.valueOf(idShop));
    }
    private  void mappingGrid(){
        gridDetail=(ExpandableHeightGridView) findViewById(R.id.gridviewBanchaydetail);
        arrayList1=new ArrayList<>();
        adapter1=new ProductGridDetailAdapter(com.example.grapfoodteam.Detail.this,arrayList1,R.layout.line_detailbanchay);
        gridDetail.setAdapter(adapter1);
        gridDetail.setExpanded(true);
        GetDuLieuBanChay(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/productbanchay.php"),String.valueOf(idShop));

    }
    private void GetDuLieuShop(String url, final String id){
        RequestQueue requestQueue= Volley.newRequestQueue(Detail.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                Picasso.get().load(jsonObject.getString("banner"))
                                        .into(baner);
                                txtNameDetail.setText(jsonObject.getString("name"));
                                txtNoteDetail.setText(jsonObject.getString("note"));
                                favorite=jsonObject.getInt("loves");
                                if(jsonObject.getInt("loves")==1)
                                    menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.tim));
                                else
                                    menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.heartwhite));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detail.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("id",id);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void UpdateShopTym(String url, final String id,final String tym){
        RequestQueue requestQueue=Volley.newRequestQueue(Detail.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Detail.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parems=new HashMap<>();
                parems.put("id",id);
                parems.put("tym",tym);
                return parems;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void GetDuLieuBanChay(String url, final String id){
        RequestQueue requestQueue= Volley.newRequestQueue(Detail.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    arrayList1.add(new ProductGridDetail(jsonObject.getInt("id"),
                                            jsonObject.getString("image"),
                                            jsonObject.getString("name"),
                                            jsonObject.getInt("price")+"đ",
                                            jsonObject.getString("note")) );
                                }
                                adapter1.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detail.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("id",id);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void GetDuLieuAll(String url, final String id){
        RequestQueue requestQueue= Volley.newRequestQueue(Detail.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    arrayList2.add(new ProductGridDetail(jsonObject.getInt("id"),
                                            jsonObject.getString("image"),
                                            jsonObject.getString("name"),
                                            jsonObject.getInt("price")+"đ",
                                            jsonObject.getString("note")));
                                }
                                adapter2.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detail.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("id",id);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void CheckCart(String url, final String id){
        RequestQueue requestQueue= Volley.newRequestQueue(Detail.this);
        StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null)
                        {

                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                for(int i=0;i<jsonArray.length();i++){
                                    layoutGioHang.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    priceTotal+=jsonObject.getInt("price")*jsonObject.getInt("qty");
                                    qtyTotal+=1;
                                }
                                txtTotalQtyGH.setText(qtyTotal+" món");
                                txtTotalPriceGH.setText(priceTotal+"đ");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detail.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> prams=new HashMap<String,String>();
                prams.put("id",id);
                return prams;
            }
        };
        requestQueue.add(stringRequest);
    }
}