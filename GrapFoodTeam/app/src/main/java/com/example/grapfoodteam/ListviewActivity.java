 package com.example.grapfoodteam;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.darwindeveloper.horizontalscrollmenulibrary.custom_views.HorizontalScrollMenuView;
import com.example.grapfoodteam.adapter.FoodAdapter;
import com.example.grapfoodteam.model.Food;
import com.example.grapfoodteam.util.Connect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



 public class ListviewActivity extends AppCompatActivity {
     GridView lvFood;
     ArrayList<Food> arrayFood;
     ArrayList<Food> array;
     FoodAdapter adapter,adapte;
     Food pp;
     String ten;
     Dialog dialog;
     int hinh;
     HorizontalScrollMenuView menu;
     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         String urlGetData = "/ltdd/webservice/AndroidTeam/linh/selectProduct.php";
         setContentView(R.layout.listview);

         TextView t=findViewById(R.id.textSan);
         ImageView back = findViewById(R.id.back);
         back.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 finish();

             }
         });
         t.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent Myinten = new Intent(ListviewActivity.this,UuDaiActivity.class);
                 startActivity(Myinten);
             }
         });

         lvFood =(GridView) findViewById(R.id.lv);
         arrayFood = new ArrayList<>();
         menu= findViewById(R.id.hz);
         initMenu();
         GetData(urlGetData);
         int mm = 4;
//GetDuLieuList("http://192.168.1.6:81/android/selectPrCondition.php",Integer.toString(mm));
         adapter = new FoodAdapter(this,R.layout.line,arrayFood);
         lvFood.setAdapter(adapter);
         clickMenu();
         lvFood.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                 Intent myIntent=new Intent(ListviewActivity.this, DetailActivity.class);
                 if(array.isEmpty())
                 {
                     myIntent.putExtra("hinh",arrayFood.get(i).getHinh());
                 }
                 else
                 {
                     myIntent.putExtra("hinh",array.get(i).getHinh());
                 }

                 startActivity(myIntent);
             }
         });


     }
     private void GetData(String url) {

         RequestQueue requestQueue = Volley.newRequestQueue(this);
         JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.GET, Connect.ConnectUrl(url), null, new Response.Listener<JSONArray>() {
             @Override
             public void onResponse(JSONArray response){

                 for(int i=0;i<response.length();i++)
                 {
                     try {
                         JSONObject object = response.getJSONObject(i);
                         arrayFood.add(new Food(
                                         object.getInt("IdSP"),
                                         object.getString("TenSP"),
                                         object.getInt("PriceSP"),
                                         object.getString("ImgSP"),
                                         object.getInt("IdShop")

                                 )
                         );
                     } catch (JSONException e) {
                         e.printStackTrace();


                     }
                 }
                 adapter.notifyDataSetChanged();

             }
         },
                 new Response.ErrorListener(){
                     @Override
                     public void onErrorResponse(VolleyError error){
                         Toast.makeText(ListviewActivity.this, error.toString(), Toast.LENGTH_LONG).show();

                     }
                 }
         );
         requestQueue.add(jsonArrayRequest);


     }
     private void GetDuLieuList(String url, final String id){

         RequestQueue requestQueue= Volley.newRequestQueue(ListviewActivity.this);
         StringRequest stringRequest =new StringRequest(Request.Method.POST, url,
                 new Response.Listener<String>() {
                     @Override
                     public void onResponse(String response) {
                         if(response!=null)
                         {
                             try {
                                 JSONArray jsonArray=new JSONArray(response);
                                 for(int i=0;i<jsonArray.length();i++)
                                 {
                                     JSONObject object=jsonArray.getJSONObject(i);
                                     array.add(new Food(
                                                     object.getInt("IdSP"),
                                                     object.getString("TenSP"),
                                                     object.getInt("PriceSP"),
                                                     object.getString("ImgSP"),
                                                     object.getInt("IdShop")

                                             )
                                     );
                                 }
                                 adapter.notifyDataSetChanged();
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }
                         }
                     }
                 }, new Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 Toast.makeText(ListviewActivity.this,error.toString(),Toast.LENGTH_LONG).show();
             }
         }){
             @Override
             protected Map<String, String> getParams() throws AuthFailureError {
                 HashMap<String,String> prams=new HashMap<String,String>();
                 prams.put("IDTYPE",id);

                 return prams;
             }
         };
         requestQueue.add(stringRequest);
     }

     public void clickMenu(){

         menu.setOnHSMenuClickListener(new HorizontalScrollMenuView.OnHSMenuClickListener() {
             @Override
             public void onHSMClick(com.darwindeveloper.horizontalscrollmenulibrary.extras.MenuItem menuItem, int position) {
                 array.clear();
                 if(position==0)
                 {
                     adapter = new FoodAdapter(ListviewActivity.this,R.layout.line,arrayFood);
                     lvFood.setAdapter(adapter);
                 }
                 else{
                     GetDuLieuList(Connect.ConnectUrl("/ltdd/webservice/AndroidTeam/linh/selectPrCondition.php"),Integer.toString(position));
                     adapter = new FoodAdapter(ListviewActivity.this,R.layout.line,array);
                     lvFood.setAdapter(adapter);
                 }


             }
         });
     }
     private void initMenu()
     {
         array= new ArrayList<>();
         menu.addItem("Tất cả",R.drawable.ic_action_all);
         menu.addItem("MilkTea",  R.drawable.trasua);
         menu.addItem("Mi cay",R.drawable.ramenn);
         menu.addItem("Kim bap",R.drawable.sushi);
         menu.addItem("Kem",R.drawable.popsicle);
         menu.addItem("Hamburger",R.drawable.burger);
         menu.addItem("Pizza",R.drawable.piza);
         menu.addItem("Khoai Tây Chiên",R.drawable.fries);

     }


 }
