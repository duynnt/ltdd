package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.grapfoodteam.R;
import com.example.grapfoodteam.model.Product;

import com.squareup.picasso.Picasso;


import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private Context context;
    private List<Product> list;
    private RecyclerViewClickListener listener;

    public ProductAdapter(Context context, List<Product> list, RecyclerViewClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener=listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView picture;
        TextView txtpromotion,txtrange,txtname,txtsale;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            picture=(ImageView) itemView.findViewById(R.id.image_picture);
            txtpromotion=(TextView) itemView.findViewById(R.id.txtpromotion);
            txtrange=(TextView) itemView.findViewById(R.id.txtrange);
            txtname=(TextView) itemView.findViewById(R.id.txtname);
            txtsale=(TextView) itemView.findViewById(R.id.txtsale);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.OnClick(view,getAdapterPosition());
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.line_product_horizontal,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Product product=list.get(position);
        Picasso.get().load(product.getPicture())
                .into(holder.picture);
        holder.txtpromotion.setText(product.getPromotion());
        holder.txtrange.setText(product.getRange());
        holder.txtname.setText(product.getName());
        holder.txtsale.setText(product.getSale());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface RecyclerViewClickListener{
        void OnClick(View v, int position);
    }
}
