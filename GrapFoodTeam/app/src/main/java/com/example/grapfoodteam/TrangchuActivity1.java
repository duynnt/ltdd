package com.example.grapfoodteam;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class TrangchuActivity1 extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getSupportActionBar().hide();

        setContentView(R.layout.activity_trangchu);
        new Timer().schedule(new TimerTask() {
            public void run() {
                startActivity(new Intent(TrangchuActivity1.this, ListviewActivity.class));
                finish();
            }
        }, 1000);
    }
}
