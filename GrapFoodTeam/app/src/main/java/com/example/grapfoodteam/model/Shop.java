package com.example.grapfoodteam.model;


public class Shop {
    private String tenquan;
    private String address;
    private String theloai;
    private String khuyenmai;
    private String thoigian;
    private String km;
    private String hinh;
    private int id;


    public Shop(String tenquan, String address, String theloai, String khuyenmai, String thoigian, String km, String hinh, int id) {
        this.tenquan = tenquan;
        this.address = address;
        this.theloai = theloai;
        this.khuyenmai = khuyenmai;
        this.thoigian = thoigian;
        this.km = km;
        this.hinh = hinh;
        this.id = id;
    }

    public String getTenquan() {
        return tenquan;
    }

    public void setTenquan(String tenquan) {
        this.tenquan = tenquan;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTheloai() {
        return theloai;
    }

    public void setTheloai(String theloai) {
        this.theloai = theloai;
    }

    public String getKhuyenmai() {
        return khuyenmai;
    }

    public void setKhuyenmai(String khuyenmai) {
        this.khuyenmai = khuyenmai;
    }

    public String getThoigian() {
        return thoigian;
    }

    public void setThoigian(String thoigian) {
        this.thoigian = thoigian;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

