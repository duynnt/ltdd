package com.example.grapfoodteam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.adapter.adapterfood;
import com.example.grapfoodteam.model.profilefood;
import com.example.grapfoodteam.util.Connect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailGanToiActivity extends AppCompatActivity {
    Button btnloc, btnbanchay, btnkmsp;
    ListView listView;
    ArrayList<profilefood> profilefoodArrayList;
    adapterfood foodAdapter;
    androidx.appcompat.widget.Toolbar toolbar;
    int nd = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gan_toi);
        anhxa();
        foodAdapter = new adapterfood(this,R.layout.dongfoodnearme,profilefoodArrayList);
        listView.setAdapter(foodAdapter);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),TrangChuActivity.class));
            }
        });
        btnloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGanToiActivity.this,LocdetalActivity.class));
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(DetailGanToiActivity.this,Detail.class));
                int idd =profilefoodArrayList.get(position).getId();
                Intent intent=new Intent(DetailGanToiActivity.this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",idd);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        });
        btnbanchay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilefoodArrayList.clear();
                foodAdapter.notifyDataSetChanged();
                getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/search.php"));
            }
        });
        btnkmsp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilefoodArrayList.clear();
                foodAdapter.notifyDataSetChanged();
                getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/searchkm.php"));
            }
        });
        Intent intent = getIntent();
        nd = intent.getIntExtra("key",1);
        if (nd < 2){
            getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/thao/shops.php"));
        }else {
            if (nd == 2 ){
                profilefoodArrayList.clear();
                foodAdapter.notifyDataSetChanged();
                getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/dexua.php"));
            }else {
                if (nd == 3){
                    profilefoodArrayList.clear();
                    foodAdapter.notifyDataSetChanged();
                    getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/noibat.php"));
                }else {
                    if (nd == 4){
                        profilefoodArrayList.clear();
                        foodAdapter.notifyDataSetChanged();
                        getData(Connect.ConnectUrl("ltdd/webservice/AndroidTeam/cart/profile/danhgia.php"));
                    }else {

                    }
                }
            }
        }

    }

    private void setSupportActionBar(Toolbar toolbar) {
    }

    private void anhxa(){
        btnkmsp = (Button) findViewById(R.id.btnkmsp);
        btnbanchay = (Button) findViewById(R.id.clickbanchay);
        btnloc = (Button) findViewById(R.id.loctheo);
        toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        listView = (ListView) findViewById(R.id.list_item);
        profilefoodArrayList = new ArrayList<>();
    }
    private void getData(String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(DetailGanToiActivity.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                profilefoodArrayList.add(new profilefood(object.getInt("id"),
                                        object.getString("name"),
                                        object.getString("note"),
                                        "4.1",
                                        "Nhập ANTIEC40 giảm 40k",
                                        "15 phút",
                                        "1,4 km",
                                        object.getString("image")));
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                       foodAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailGanToiActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }
        );
        requestQueue.add(jsonArrayRequest);
    }
}