package com.example.grapfoodteam;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.grapfoodteam.adapter.ShopAdapter;
import com.example.grapfoodteam.model.Shop;
import com.example.grapfoodteam.model.Users;
import com.example.grapfoodteam.util.Connect;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Yeuthich extends AppCompatActivity {
    ListView listView;
    Context context;
    ImageView picturelist;
    TextView TenQuan,Address,theloai,khuyenmai,thoigian,km;
    ArrayList<Shop> shop_data ;
    ShopAdapter shopAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_yeuthich);
        //getviews
        listView = findViewById(R.id.list_yeuthich);

        //add shop Data
        shop_data = new ArrayList<>();
        getData();
        shopAdapter = new ShopAdapter(context,shop_data,R.layout.row_shop);
        listView.setAdapter(shopAdapter);
        ImageView back = (ImageView) findViewById(R.id.back_yeuthich);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Yeuthich.this, TrangChuActivity.class);
                startActivity(intent);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int idd =shop_data.get(position).getId();
                Intent intent=new Intent(Yeuthich.this,Detail.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",idd);
                intent.putExtra("idShop",bundle);
                startActivity(intent);
            }
        });
    }

    private void getData(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Connect.ConnectUrl("ltdd/webservice/AndroidTeam/Yeuthich/connect.php"), null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        if(response.length()==0){
                            Toast toast= Toast.makeText(getApplicationContext(),
                                    Html.fromHtml("<font color='#00B14F' ><b>Không có quán yêu thích !</b></font>"), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER| Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                        else {
                            for (int i=0; i < response.length(); i++){
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    shop_data.add(new Shop(
                                            jsonObject.getString("nameShops"),
                                            "",
                                            jsonObject.getString("address"),
                                            "Giảm 20%",
                                            "45 phút",
                                            "2km",
                                            jsonObject.getString("imagesBanner"),
                                            jsonObject.getInt("idShops")
                                    ));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            shopAdapter.notifyDataSetChanged();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "loi!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

}