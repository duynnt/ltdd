package com.example.grapfoodteam.adapter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.grapfoodteam.R;
import com.example.grapfoodteam.model.SanPham;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context context;
    private List<SanPham> sanPhamList;

    public MyAdapter(Context context, List<SanPham> sanPhamList) {
        this.context = context;
        this.sanPhamList = sanPhamList;
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements ViewGroup.OnCreateContextMenuListener{
        TextView txtTen,txtGia;
        ImageView imgHinh;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTen=(TextView) itemView.findViewById(R.id.textviewTen);
            txtGia=(TextView) itemView.findViewById(R.id.textviewgia);
            imgHinh=(ImageView) itemView.findViewById(R.id.imageviewHinh);

        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(context).inflate(R.layout.layout_dong,parent,false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        SanPham sanPham=sanPhamList.get(position);
        holder.txtTen.setText(sanPham.getTen());
        holder.txtGia.setText(sanPham.getGia());
        holder.imgHinh.setImageResource(sanPham.getHinh());
    }

    @Override
    public int getItemCount() {
        return sanPhamList.size();
    }
}
