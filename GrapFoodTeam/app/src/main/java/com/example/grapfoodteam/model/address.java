package com.example.grapfoodteam.model;

public class address {
    public  int id;
    public String addressName;
    public String address;
    public String addressDetails;
    public String addressNote;

    public address(int id, String addressName, String address, String addressDetails, String addressNote) {
        this.id = id;
        this.addressName = addressName;
        this.address = address;
        this.addressDetails = addressDetails;
        this.addressNote = addressNote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDetails() {
        return addressDetails;
    }

    public void setAddressDetails(String addressDetails) {
        this.addressDetails = addressDetails;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }
}
